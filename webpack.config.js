let Encore = require('@symfony/webpack-encore');
const AppModule = require('./common/frontend/AppModule.js');
const glob = require('glob');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

// Подключение файлов модулей
new AppModule('app-admin/frontend', __dirname);


/*// Подключает дополнительные файлы конфигурации webpack
// Пример: require('./frontend/admin/app/webpack.config.js');

const customConfigs = [
    'frontend/admin',
];

glob.sync('{' + customConfigs.join() + '}/webpack.config.js').forEach(function (moduleConfigFile) {
    require(path.join(__dirname, moduleConfigFile));
});*/


Encore
// the project directory where compiled assets will be stored
    .setOutputPath(Encore.isProduction() ? 'web/build-prod/' : 'web/build/')
    // the public path used by the web server to access the previous directory
    .setPublicPath('/build')
    // empty the outputPath dir before each build
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    // uncomment to create hashed filenames (e.g. app.abc123.css)
    .enableVersioning(!Encore.isProduction())

    .addAliases({
        node_modules: path.resolve(__dirname, 'node_modules'),
        vendor: path.resolve(__dirname, 'vendor'),
        tmp: path.resolve(__dirname, 'tmp')
    })

    // Подключаем общий frontend
    .addEntry('common/frontend/js/script', './common/frontend/js/imports.js')
    .addStyleEntry('common/frontend/css/style', './common/frontend/css/imports.scss')

    // Подключаем frontend приложения index
    .addEntry('app-index/frontend/js/script', './app-index/frontend/js/imports.js')
    .addStyleEntry('app-index/frontend/css/style', './app-index/frontend/css/imports.scss')

    // Включаем сохранение структуры каталогов при импорте изображений и шрифтов в папку build
    .configureFilenames({
        images: '[path][name].[hash:8].[ext]',
        fonts: '[path][name].[hash:8].[ext]'
    })

    .enableReactPreset()

    // use Sass/SCSS files
    .enableSassLoader()

    // use LESS files
    .enableLessLoader()

    // legacy applications that require $/jQuery as a global variable
    .autoProvidejQuery()

    .addPlugin(
        new CopyWebpackPlugin([
            // Копируем CKEditor в директорию web/build/ckeditor. Необходимо для работы FOSCKEditorBundle
            // CKEditor устанавливается через npm, команду "php bin/console ckeditor:install" запускать не нужно
            { from: 'node_modules/ckeditor', to: 'common/ckeditor' },
            // Копируем плагин загрзки файлов
            { from: 'node_modules/filepond/dist', to: 'common/filepond' },
        ])
    )
    // Babel config
    .configureBabel(function(babelConfig) {
        // add additional presets
        babelConfig.presets.push('stage-3');
        babelConfig.presets.push('env');
        babelConfig.plugins.push('transform-runtime');
    })

;

// Подключаем временные файлы с изображениями
// Encore.addEntry('tmp/images', './tmp/images.js');

module.exports = Encore.getWebpackConfig();
