# beads-market

Сгенерировать классы моделей и перезатереть все
```
bin/console make:entity --regenerate --overwrite App\\Entity
```

Сгенерировать классы моделей не перезатирая методы
```
bin/console make:entity --regenerate App\\Entity
```

Создать файлы разметки на основе базы данных.
Будут созданы файлы вида "Bead.orm.yml".
```
bin/console doctrine:mapping:import App\\Entity yaml --path backend/common/db/Schema
```

Запустить сборку webpack
```
./node_modules/.bin/encore dev
```

Загрузить фикстуры
```
bin/admin doctrine:fixtures:load
```



## Продуктив

Включить технические работы:
```
# В файле .env
TECHNICAL_WORKS='Технические работы'
```

Путь к файлу autoload.php и .env
```
# В файле .htaccess
SetEnv PATH_AUTOLOAD_PHP /../vendor/beads-market/vendor/autoload.php
SetEnv PATH_DOT_ENV /../vendor/beads-market/.env
```

Выгрузка файлов:
```
Файлы из директории web
~/mororo.ru/

Файлы проекта
~/vendor/beads-market/
```

fabric. список команд
```
fab  -f devtools/deploy/run.py --list
```

Временная авторизация:
```
qwaso@qip.ru / nuttertools
```
