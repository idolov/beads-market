# Приложение выгрузки

Установка fabric
```
pip install fabric3
```

Настройка окружения программы выгрузки
```
cp devtools/deploy/env.py-dist devtools/deploy/env.py
nano devtools/deploy/env.py
```

Список команд
```
fab -f devtools/deploy/run.py --list
```

Выгрузка файла или директории на удаленный сервер
```
fab -f devtools/deploy/run.py up:path=bin
```

Полная выгрузка проекта
```
fab -f devtools/deploy/run.py dep
```

Выполнить "composer install"
```
fab -f devtools/deploy/run.py ci
```

Восстановить права доступа к файлам и директориям
```
fab -f devtools/deploy/run.py fixp
```

Очистить кэш
```
fab -f devtools/deploy/run.py сс
```
