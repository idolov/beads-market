# -*- coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
from fabric.api import *
from fabric.colors import *
import os.path
from env import *
from lib.config import *
from lib.functions import *

# проверим, что работаем не от рута
if "root" == env.user:
    abort(red("Работа с fabric под пользователем root не возможна!"))

###############################################
#                   Задачи                    #
###############################################

@task(aliases=['dep'])
def deploy():
    ''' Полная выгрузка проекта на удаленный хост '''
    init()
    prepare_env()
    for relpath in env.deploy_project_paths:
        upload(relpath)

    composer_ins()
    fix_permissions()
    clear_cache()
    print(green("Развертывание завершено."))

@task(aliases=['ci'])
def composer_ins():
    ''' Выполняет команду "composer install" на удаленном сервере '''
    init()
    with cd(env.host_composer_dir):
        composer_install()

    print(green("Выполнен composer install"))

@task(aliases=['cc'])
def clear_cache(app='all'):
    ''' Очистка кешей приложения
        :param app: Имя приложения app, index (если указано all - очистит все кэши)
    '''
    init()
    with cd(env.host_commands_dir):
        if app =='all':
            for command_name in env.clear_cache_commands:
                clear_cache_command(command_name)
        else:
            clear_cache_command(app)

@task(aliases=['fixp'])
def fix_permissions():
    ''' Установка нужных прав на файлы и папки '''
    init()
    prepare_env()
    default_permissions()
    x_permissions()
    rw_permissions()
    print(green("Права доступа назначены"))

@task(aliases=['up'])
def upload(path):
    ''' Копирование файлов на удаленный сервер
        :param path: Путь к файлу или директории, которая должна быть закружена на удаленный хост
    '''
    init()
    prepare_env()
    src = get_abs_project_path(path)

    if not os.path.exists(src):
        abort(red("Not found  - %s" % src))

    if (not can_upload(src)) and not (src in env.deploy_root_dirs):
        print(red("Ignored - %s" % path))
        return

    dst = get_abs_host_path(path)
    if not dst:
        print(red("Ignored - %s" % path))
        return

    if os.path.isdir(src):
        rm_on_host(dst)
    upload_files(src, dst)

# todo Сделать команду на получения дампа базы данных
# run('mysqldump [options] | gzip > outputfile.sql.gz')
# get('outputfile.sql.gz')
