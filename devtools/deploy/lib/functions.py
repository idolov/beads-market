# -*- coding: utf-8 -*-
from fabric.api import *
from fabric.colors import *
import os.path
import re

# Признак, что конфигурация уже инициализирована
env.init = False

# Признак, что переменные окружени уже подготовлены
env.prepare_env = False

###############################################
#                  Функции                    #
###############################################

def init():
    ''' Инициализирует конфигурацию '''
    if env.init == False:
        env.prefix = 'umask 0002'
        env.sudo_user = env.deploy_user
        env.init = True

def prepare_env():
    ''' Подготавливает дополнительные переменные окружения '''
    if env.prepare_env == False:
        init_deploy_project_paths()
        init_deploy_paths_patterns()
        init_ignore_paths_patterns()
        init_deploy_root_dirs()
        env.prepare_env == True

def get_abs_project_path(path):
    ''' Получает абсолютный путь к файлу проекта
        :param path: Путь к файлу или директории относительно локальной директории проекта
    '''
    if not path:
        abort(red("Specify the path relative to the project directory"))

    if os.path.isabs(path):
        src = path
    else:
        src = os.path.realpath(os.path.join(env.project_dir, path))

    if not re.match(env.project_dir, src):
        abort(red("Out of project  - %s" % src))

    return src

def get_abs_host_path(path):
    ''' Получает абсолютный путь к файлу на удаленном сервере
        :param path: Путь к файлу или директории относительно локальной директории проекта
    '''
    base_tail = split_deploy_path(path)
    base_relpath = base_tail[0]
    tail_relpath = base_tail[1]
    if base_relpath:
        src = get_abs_project_path(base_relpath)
        path = base_relpath
    else:
        src = get_abs_project_path(path)

    root = split_path(path)[0]
    dst = ""

    if root == "web":
        if path in env.deploy_project_paths:
            dst = os.path.join(env.host_frontend_dir, env.deploy_project_paths[path])
        else:
            #  Путь относительно директории web
            webDirPath = os.path.join(env.project_dir, 'web')
            relpath = os.path.relpath(src, webDirPath)
            dst = os.path.join(env.host_frontend_dir, relpath)
    else:
        if path in env.deploy_project_paths:
            dst = os.path.join(env.host_backend_dir, env.deploy_project_paths[path])
        else:
            dst = os.path.join(env.host_backend_dir, path)

    if tail_relpath:
        dst = os.path.realpath(dst + tail_relpath)

    return dst

def init_deploy_project_paths():
    ''' Проверяет пути выгружаемых файлов и директорий, указанные в конфигурации env.deploy_project_paths.
        Оставляет только годные
    '''
    rel_paths = {}
    for relpath in env.deploy_project_paths:
        if os.path.isabs(relpath):
            print(red('Invalid path "' + relpath + '" in "env.deploy_project_paths". Specify relative path.'))
            continue
        path = os.path.join(env.project_dir, relpath)
        if os.path.exists(path):
            rel_paths[relpath] = env.deploy_project_paths[relpath]
        else:
            print(red('Invalid path "' + relpath + '" in "env.deploy_project_paths". Not found.'))

    env.deploy_project_paths = rel_paths

def init_deploy_paths_patterns():
    ''' Инициализирует reg-exp паттерны для проверки файлов проекта, подходят ли они для выгрузки '''
    env.deploy_paths_patterns = []
    for relpath in env.deploy_project_paths:
        pattern = get_pattern(relpath)
        if pattern:
            env.deploy_paths_patterns.append(pattern)

def init_ignore_paths_patterns():
    ''' Инициализирует reg-exp паттерны для проверки файлов проекта, должны ли они быть проигнорированы при выгрузке '''
    env.ignore_paths_patterns = []
    for relpath in env.ignore_project_paths:
        pattern = get_pattern(relpath)
        if pattern:
            env.ignore_paths_patterns.append(pattern)

def init_deploy_root_dirs():
    ''' Получает список директорий в корне проекта, файлы которых будут выгружаться на продуктив '''
    env.deploy_root_dirs = []
    for relpath in env.deploy_project_paths:
        path = os.path.join(env.project_dir, relpath)
        if os.path.isdir(path):
            root = split_path(relpath)[0]
            rootPath = os.path.join(env.project_dir, root)
            if not (rootPath in env.deploy_root_dirs):
                env.deploy_root_dirs.append(rootPath)

def get_sorted_deploy_project_paths():
    ''' Получает отсортированный список выгружаемых путей. Сортировка по уменьшению глубины вложенности '''
    ordered_paths = []
    pathlen_list = {}

    for relpath in env.deploy_project_paths:
        splitpath = split_path(relpath)
        pathlen = len(splitpath)
        if pathlen not in pathlen_list:
            pathlen_list[pathlen] = []
        pathlen_list[pathlen].append(relpath)

    for pathlen in sorted(pathlen_list.keys(), reverse = True):
        for relpath in pathlen_list[pathlen]:
            ordered_paths.append(relpath)
    return ordered_paths

def get_pattern(relpath):
    ''' Получает reg-exp паттерн для проверки файлов проекта '''
    path = get_abs_project_path(relpath)
    pattern = ''
    if os.path.isdir(path):
        pattern = "^" + path
    elif os.path.isfile(path):
        pattern = "^" + path + "$"

    return pattern

def split_path(path):
    ''' Разбивает путь на элементы иерархии (root/dir/file -> ["root", "dir", "file"])
        :param path: Путь относительный или абсолютный
    '''
    pathParts = []
    while 1:
        path, folder = os.path.split(path)

        if folder != "":
            pathParts.append(folder)
        else:
            if path != "":
                pathParts.append(path)
            break

    pathParts.reverse()
    return pathParts

def split_deploy_path(path):
    ''' Разбивает путь по принципу (base_root/base_dir/tail -> ["base_root/base_dir", "tail"])
        :param path: Путь относительный или абсолютный
    '''
    path = get_abs_project_path(path)
    base_relpath = ''
    tail_relpath = ''
    for dep_relpath in get_sorted_deploy_project_paths():
        pattern = get_pattern(dep_relpath);
        if re.match(pattern, path):
            base_relpath = dep_relpath
            tail_relpath = re.sub(pattern, '', path)
            break

    return [base_relpath, tail_relpath]

def is_ignore(abspath):
    ''' Проверяет путь, должен ли он быть проигнорирован при выгрузке на продуктив
        :param abspath: Абсолютный путь к файлу проекта
    '''
    if not os.path.isabs(abspath) or not os.path.exists(abspath):
        return True
    if os.path.isfile(abspath):
        fileName = os.path.basename(abspath)
        if fileName in env.ignore_project_filenames:
            return True

    result = False
    for pattern in env.ignore_paths_patterns:
        if re.match(pattern, abspath):
            result = True

    return result

def can_upload(abspath):
    ''' Проверяет путь, может ли он быть выгружен на продуктив
        :param abspath: Абсолютный путь к файлу проекта
    '''
    if not os.path.isabs(abspath) or not os.path.exists(abspath) or is_ignore(abspath):
        return False

    result = False
    for pattern in env.deploy_paths_patterns:
        if re.match(pattern, abspath):
            result = True

    return result

def upload_files(src, dst):
    ''' Выгружает файлы на продуктив
        :param src: Абсолютный путь к файлу проекта
        :param dst: Абсолютный путь на удаленном сервере, по которому будет выгружен файл проекта
    '''
    if not src or not dst:
        return

    src = os.path.realpath(src)
    dst = os.path.realpath(dst)

    if not can_upload(src):
        print(red("ignored - %s" % src))
        return

    if os.path.isdir(src):
        mkdir_on_host(dst)
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            upload_files(s, d)
    elif os.path.isfile(src):
        put(src, dst)
        return
    else:
        print(red("ignored - %s" % src))
        return

def rm_on_host(abspath):
    ''' Удаляет файл или директорию на удаленном сервере
        :param abspath: Абсолютный путь к файлу или директории на удаленном сервере
    '''
    if os.path.isabs(abspath):
        with settings(warn_only=True, ok_ret_codes=[0, 1]):
            run("rm -rf %s" % abspath)

def mkdir_on_host(abspath):
    ''' Создает директорию на удаленном сервере
        :param abspath: Абсолютный путь к директории на удаленном сервере
    '''
    if os.path.isabs(abspath):
        with settings(warn_only=True, ok_ret_codes=[0, 1]):
            run("mkdir -p %s" % abspath)

def composer_install():
    ''' Установка/обновление зависимостей из файла composer.json проэкта с учётом composer.lock '''
    composer_options = '--no-progress --no-ansi --no-scripts --no-dev'
    run("test -f composer.json && composer install  %s || echo 'Composer is not used'" % composer_options)

def clear_cache_command(command_name):
    ''' Выполняет команду очистки кэша
        :param command_name: Имя команды для очистки кэша (см. env.clear_cache_commands)
     '''
    if command_name not in env.clear_cache_commands:
        abort(red('Command not found'))

    with settings(warn_only=True, ok_ret_codes=[0, 1]):
        run(env.clear_cache_commands[command_name])

def default_permissions():
    ''' Устновка стандартных прав для директорий и файлов проекта '''
    for path in env.fix_permissions_dirs:
        run("chown -R :%s %s" % (env.file_group, path))
        run("find %s -type d -exec chmod %s {} \;" % (path, env.dir_access))
        run("find %s -type f -exec chmod %s {} \;" % (path, env.file_access))

def rw_permissions():
    ''' Установка прав для директорий, в которые должен писать web-сервер '''
    for directory in env.webserver_rw_dirs:
        path = get_abs_host_path(directory)
        mkdir_on_host(path)
        run("chmod 777 %s -R" % path)

def x_permissions():
    ''' Установка прав на выполнение '''
    for executable in env.executables:
        path = get_abs_host_path(executable)
        run("chmod +x %s" % path)
