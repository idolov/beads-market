# -*- coding: utf-8 -*-
from fabric.api import env
import os.path

###############################################
#                  Настройки                  #
###############################################

# Локальная директория проекта
env.project_dir = os.path.realpath(os.path.dirname(__file__) + "/../../..")

# Директория на удаленном хосте, где лежат front-контроллеры и прочие frontend-файлы
env.host_frontend_dir = "/home/fedonisg/mororo.ru"

# Директория на удаленном хосте, где лежат backend-файлы
env.host_backend_dir = "/home/fedonisg/vendor/beads-market"

# Путь к директории на удаленном хосте, где лежат файлы composer.json и composer.lock
env.host_composer_dir = env.host_backend_dir

# Пути директорий и файлов, которые могут быть загружены на продуктивный сервер и их относительные пути на сервере
# Пути указываются относительно директории проекта
env.deploy_project_paths = {
    'web/build-prod': 'build',
    'web/images': 'images',
    'web/admin.php': 'admin.php',
    'web/index.php': 'index.php',
    'web/.htaccess.prod': '.htaccess',
    'backend': 'backend',
    'bin': 'bin',
    'composer.json': 'composer.json',
    'composer.lock': 'composer.lock'
}

# Названия файлов, которые не могут быть загужены на продуктив
env.ignore_project_filenames = [
    '.gitignore',
    '.htaccess'
]

# Пути к файлам и директориям проекта, которые не могут быть загружены на продуктив
env.ignore_project_paths = [
    'backend/index/cache',
    'backend/index/log',
    'backend/admin/log',
    'backend/admin/cache'
]

# Команды очистки кэша для каждого приложения проекта
env.clear_cache_commands = {
    'admin': './bin/admin cache:clear',
    'index': './bin/console cache:clear',
}

# Директории к которым вебсерверу нужны права на запись
env.webserver_rw_dirs = [
    'backend/index/cache',
    'backend/index/log',
    'backend/admin/log',
    'backend/admin/cache'
]

# Директории в которых указываем дефолтные права доступа
env.fix_permissions_dirs = [
    env.host_frontend_dir,
    env.host_backend_dir,
]

# Путь к директории в которой лежат исполняемые файлы
env.host_commands_dir = env.host_backend_dir

# Исполняемые файлы
env.executables = [
    'bin/console',
    'bin/admin',
]

# права доступа
env.dir_access = "755"
env.file_access = "644"
env.file_group = "fedonisg"
env.deploy_user = "fedonisg"
env['sudo_prefix'] += ' -H '
