<?php
namespace App\modules\profile\controllers;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OrderController extends Controller
{
    /**
     * @Route("/profile/order", name="profile_order_index")
     */
    public function indexAction()
    {
        return $this->render('profile/order/index.html.twig');
    }
}
