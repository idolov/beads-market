<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class CategoryFixtures extends Fixture
{
    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        /** @var NestedTreeRepository $repo */
        $repo = $manager->getRepository(Category::class);

        $category1 = new Category();
        $category1->setName('Категория 1');

        $category2 = new Category();
        $category2->setName('Категория 2');

        $repo->persistAsFirstChild($category1)
            ->persistAsFirstChildOf($category2, $category1);

        $manager->flush();
    }
}
