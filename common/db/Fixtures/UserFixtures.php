<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\Client;
use App\Entity\Person;
use App\Entity\User;
use AppAdmin\bundles\SecurityBundle\Model\AdminUser;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    const ADMIN_LOGIN = 'qwaso@qip.ru';
    const ADMIN_PASSWD = 'nuttertools';

    /** @var UserPasswordEncoderInterface */
    public $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @inheritdoc
     */
    public function load(ObjectManager $manager)
    {
        $this->loadAdmin($manager);
    }

    /**
     * Загружает пользователя с правами администратора
     * @param ObjectManager $manager
     */
    public function loadAdmin(ObjectManager $manager)
    {
        $person = new Person();
        $person->setSurname('Смит');
        $person->setName('Джон');
        $person->setPatronymic('Иванович');
        $manager->persist($person);
        $manager->flush();

        $admin = new Admin();
        $admin->setPerson($person);
        $admin->setRoles(['ROLE_ADMIN']);
        $manager->persist($admin);
        $manager->flush();

        $client = new Client();
        $client->setPerson($person);
        $manager->persist($client);
        $manager->flush();

        $user = new User();
        $adminUser = new AdminUser($user);

        $user->setEmail(static::ADMIN_LOGIN);
        $user->setPassword($this->encoder->encodePassword($adminUser, static::ADMIN_PASSWD));
        $user->setAdmin($admin);
        $user->setClient($client);
        $manager->persist($user);
        $manager->flush();
    }
}
