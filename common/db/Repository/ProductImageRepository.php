<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductImage[]    findAll()
 * @method ProductImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductImage::class);
    }

    /**
     * Помечает изображение товара как основное
     * @param ProductImage $productImage Изображение товара
     * @param ObjectManager $objectManager
     */
    public function markAsMain(ProductImage $productImage, ObjectManager $objectManager)
    {
        // Обнулим признак is_main у прошлого основного изображения
        $this->_em->createQueryBuilder()
            ->update(ProductImage::class, 'pi')
            ->set('pi.isMain', 0)
            ->where('pi.product = :product AND pi.isMain = 1')
            ->setParameter('product', $productImage->getProduct())
            ->getQuery()
            ->execute();

        $productImage->setIsMain(true);
        $objectManager->flush();
    }

    /**
     * Получает изображения товара
     * @param Product $product
     * @return mixed
     */
    public function getImagesByProduct(Product $product)
    {
        return $this->_em->createQueryBuilder()
            ->select('pi')
            ->from(ProductImage::class, 'pi')
            ->orderBy('pi.isMain', 'DESC')
            ->where('pi.product = :product')
            ->setParameter('product', $product)
            ->getQuery()
            ->execute();
    }
}
