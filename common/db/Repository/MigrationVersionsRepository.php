<?php

namespace App\Repository;

use App\Entity\MigrationVersions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MigrationVersions|null find($id, $lockMode = null, $lockVersion = null)
 * @method MigrationVersions|null findOneBy(array $criteria, array $orderBy = null)
 * @method MigrationVersions[]    findAll()
 * @method MigrationVersions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MigrationVersionsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MigrationVersions::class);
    }
}
