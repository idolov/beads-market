<?php

namespace App\Entity;

class ProductImage
{
    private $id;

    private $path;

    private $name;

    private $mimeType;

    private $size;

    private $product;
    
    private $isMain = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Получает путь, куда сохраняются изображения
     * @param string $defaultPath
     * @return string
     */
    public function getUploadsPath($defaultPath)
    {
        return $defaultPath . "/catalog/product/{$this->getProduct()->getId()}/";
    }

    /**
     * Получает URL изображения
     * @return string
     */
    public function getUrl()
    {
        return "/uploads/catalog/product/{$this->getProduct()->getId()}/{$this->getName()}";
    }

    public function getIsMain(): ?bool
    {
        return $this->isMain;
    }

    public function setIsMain(bool $isMain): self
    {
        $this->isMain = $isMain;

        return $this;
    }
}
