<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MigrationVersionsRepository")
 */
class MigrationVersions
{
    private $version;

    public function getVersion(): ?string
    {
        return $this->version;
    }
}
