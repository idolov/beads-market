<?php

namespace App\Entity;

/**
 * Клиентский аккаунт, содержит данные о человеке, как о клиенте интернет магазина
 * (например номер лицевого счета, данные о начислениях бонусов, баланс, покупки, комментарии, отзывы,
 * вся деятельность в рамках интернет магазина, привязывается именно к нему)
 */
class Client
{
    /** @var int */
    private $id;

    /** @var Person Персональные данные */
    private $person;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Получает персональные данные
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * Заполняет персональные данные
     * @param Person|null $person
     * @return Client
     */
    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }
}
