<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Сущность - пользователь. Хранит, все, что связано c аутентификацией пользователя,
 * (данные для авторизации хранятся в аккаунтах client и admin)
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /** @var int */
    private $id;

    /** @var string Email для аутентификации */
    private $email;

    /** @var string Пароль в зашифрованном виде */
    private $password;

    /** @var Admin Аккаунт администратора */
    private $admin;

    /** @var Client Клиентский аккаунт */
    private $client;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Получает email для аутентификации
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Устанавливает email для аутентификации
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Получает пароль
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Заполняет пароль
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Получает аккаунт администратора, (если он есть) для авторизации пользователя в приложении admin
     * @return Admin|null
     */
    public function getAdmin(): ?Admin
    {
        return $this->admin;
    }

    /**
     * Заполняет аккаунт администратора
     * @param Admin|null $admin
     * @return User
     */
    public function setAdmin(?Admin $admin): self
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Получает клиентский аккаунт, (если он есть) для авторизации пользователя в приложении интернет-магазина (index)
     * @return Client|null
     */
    public function getClient(): ?Client
    {
        return $this->client;
    }

    /**
     * Заполняет клиентский аккаунт
     * @param Client|null $client
     * @return User
     */
    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }
}
