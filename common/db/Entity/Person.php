<?php

namespace App\Entity;

/**
 * Хранит персональные данные человека (Рост, вес, пол, телефон для связи, адрес проживания,
 * дата рождения, любимый цвет и прочее).
 * Одна персона может иметь несколько аккаунтов (как для админки, так и для каталога).
 */
class Person
{
    /** @var int */
    private $id;

    /** @var string Фамилия*/
    private $surname;

    /** @var string Имя */
    private $name;

    /** @var string Отчество */
    private $patronymic;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Получает фамилию
     * @return null|string
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * Заполняет фамилию
     * @param null|string $surname
     * @return Person
     */
    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Получает имя
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Заполняет имя
     * @param null|string $name
     * @return Person
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Получает отчество
     * @return null|string
     */
    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    /**
     * Заполняет отчество
     * @param null|string $patronymic
     * @return Person
     */
    public function setPatronymic(?string $patronymic): self
    {
        $this->patronymic = $patronymic;

        return $this;
    }
}
