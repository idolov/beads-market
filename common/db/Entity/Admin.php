<?php

namespace App\Entity;

/**
 * Аккаунт приложения admin.
 * Cодержит все что касается аккаунта в админке, (права доступа, аватарка, псевдоним),
 * никаких телефонов (рабочие телефоны должны быть в сущности executor  и доступны через сущность person).
 * В таблице admin исключительно то, что связано с интерфейсом админки.
 * Executor может и не иметь аккаунта в admin, соответственно ему и не нужен аватар для этого интефейса,
 * но у него есть служебный телефон.
 */
class Admin
{
    /** @var int */
    private $id;

    /** @var string Права доступа */
    private $roles;

    /** @var Person Персональные данные */
    private $person;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Получает права доступа
     * @return array
     */
    public function getRoles(): array
    {
        return explode('|', $this->roles);
    }

    /**
     * Заполняет права доступа
     * @param array $roles
     * @return Admin
     */
    public function setRoles(array $roles): self
    {
        $this->roles = implode('|', $roles);

        return $this;
    }

    /**
     * Получает персональные данные
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * Заполняет персональные данные
     * @param Person|null $person
     * @return Admin
     */
    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }
}
