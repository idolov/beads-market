# Здесь размещаем миграции в упрощенном виде чтобы можно было в любой момент дропнуть базу и воссоздать ее снова. Временная мера, пока не запилю нормальные миграции

create table bead (id INT(11) UNSIGNED NOT NULL primary key AUTO_INCREMENT, product_id INT(11) unsigned default null comment 'ИД товара. FK product.id');

create table product (id INT(11) UNSIGNED NOT NULL primary key AUTO_INCREMENT, name varchar(255) not null comment 'Название продукта');
