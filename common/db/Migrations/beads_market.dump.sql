-- MySQL dump 10.15  Distrib 10.0.35-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: beads_market
-- ------------------------------------------------------
-- Server version	10.0.35-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned DEFAULT NULL,
  `roles` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Права доступа (роли)',
  PRIMARY KEY (`id`),
  KEY `admin_person_FK` (`person_id`),
  CONSTRAINT `FK_880E0D76217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,1,'ROLE_ADMIN');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bead`
--

DROP TABLE IF EXISTS `bead`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bead` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned DEFAULT NULL COMMENT 'ИД товара. FK product.id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bead`
--

LOCK TABLES `bead` WRITE;
/*!40000 ALTER TABLE `bead` DISABLE KEYS */;
/*!40000 ALTER TABLE `bead` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `client_person_FK` (`person_id`),
  CONSTRAINT `FK_C7440455217BBB47` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT 'Название страны',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Страны с которыми мы ведем торговлю или делаем там закупки';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Россия'),(2,'Китай');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES (''),('20190121131303');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `surname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Фамилия',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Имя',
  `patronymic` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Отчество',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,'Долгов','Иван','Вадимович');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Название продукта',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00' COMMENT 'Цена',
  `notice` varchar(255) DEFAULT NULL COMMENT 'Краткое описание товара',
  `description` text COMMENT 'Подробное описание товара',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Тестовый продукт',0.00,NULL,'<h3 style=\"text-align: center;\">Заголовок</h3>\r\n\r\n<p>Список</p>\r\n\r\n<ul>\r\n	<li>фывфы</li>\r\n	<li>фывфывф</li>\r\n	<li>фыв</li>\r\n</ul>'),(2,'Бусина голубая',0.00,'<p><strong>Lorem Ipsum</strong> - это текст, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм</p>','<h2>Что такое Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong> - это текст-, &quot;часто&quot; используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Что такое Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong> - это текст-, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Что такое Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong> - это текст-, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Что такое Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong> - это текст-, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Что такое Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong> - это текст-, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Что такое Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong> - это текст-, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</p>'),(4,'Тестовый продуктbbbbbb',0.00,'<p>sdfgsgfsdfgsdfg</p>','<p>asdasd</p>\r\n\r\n<p><strong>asdasd</strong></p>'),(5,'Последний добавленный',100.00,'Анонсик',NULL),(6,'ывыва',123.00,'<p>йцуйцу</p>','<p>йцуйцу</p>'),(7,'йцуйцу',123.00,'<p>йццйу</p>','<p>йцу</p>'),(8,'Какая',123.00,'<p><strong>йцуйцу</strong></p>','<p>йцу</p>'),(9,'123',321.00,'<p><strong>йцуйцу</strong></p><p><strong>йцуйцу</strong></p>','<p>йцуйцу</p>'),(10,'1212',123.00,'<p>йцуйцу</p>','<p>цйуу</p>'),(11,'123',123.00,'<p>123</p>','<p>123</p>'),(12,'123',213.00,'<p>йцу</p>','<p>йцу</p>'),(13,'1231',123.00,'<p>ууу</p>','<p>ууу</p>'),(14,'321',321.00,'<p>йцу</p>','<p>йцу</p>'),(15,'213123',123.00,'<p>фыв</p>','<p>фыв</p>'),(16,'321',321.00,'<p>фыв</p>','<p>фыв</p>'),(17,'321',321.00,'<p>йфыв</p>','<p>фыв</p>'),(18,'1231',123.00,'<p>у</p>','<p>у</p>'),(19,'фыв',123.00,'<p>ййцу</p>','<p>йцу</p>'),(20,'йцу',123.00,'<p>фыв</p>','<p>фы</p>'),(21,'asdf',500.00,'<p>asd</p>','<p>asdf</p>');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT 'Наименование поставщика',
  `description` text COMMENT 'Описание поставщика',
  `url` text COMMENT 'Ссылка на сайт',
  `country_id` int(10) unsigned DEFAULT NULL,
  `address` text COMMENT 'Адрес поставщика',
  `phones` text COMMENT 'Телефоны для связи',
  PRIMARY KEY (`id`),
  KEY `supplier_countries_FK` (`country_id`),
  CONSTRAINT `FK_9B2A6C7EF92F3E70` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` VALUES (1,'Поставщик 2','Текстовый блок\r\nОписание поставщика','https://google.ru',1,'Челябинск, Толбухина 5-395','89511134745 - Ваня\r\n89511124340 - Надя'),(2,'Тестовый поставщик','Описание тестового поставщика','https://yandex.ru',1,'Москва, Красная площадь',NULL),(5,'Тестовый поставщик','Описание\r\nПросто текст',NULL,1,'Сибирь','99999999'),(7,'Новый',NULL,NULL,1,NULL,NULL),(8,'фудзияма','Китайский поставщик косметики',NULL,2,NULL,NULL),(9,'ццукцукцук','цукцу\r\nкцук\r\nцук','цкцукцу',1,'цукцу','кцук'),(10,'цуйцу','цыфкцук','цукцфк',2,'цукфц','цфукцфк'),(11,'цукфцк','фцукфцукфцук\r\nцфукфцк\r\nфцку\r\nфцук','цфукцфкц',1,'цукф','к'),(12,'фцукцфу','цфукфцук','фцук',1,'фцк','фцук\r\nфцук\r\nцфук\r\nцфук'),(13,'фцукфц','фцукфцук','фцук',1,'фцук','фцк'),(14,'фцукцфуцук','цфукцфук','цукцфук',1,'цук','цфукцфк'),(15,'цукфцкфцу','цфукфц','укцфк',1,'фцкцфк','цфукфцук'),(16,'фцукфцук','фцукф','цуфкцук',1,'фцукф','цкфцкук'),(17,'цукфцку','фцукфцук','фцукфцук',1,'фцукфцукфц','цукфцкцфу'),(18,'фцукфцку','фцукфцук','фцук',1,'фцк','цфк'),(19,'фкфцук','цук','фцук',1,'цфукфцк','фцукфцк'),(20,'ZZ','Z',NULL,1,'Z',NULL),(21,'as','as','as',1,'as','as'),(22,'qs','q','qs',1,'qs','qs'),(23,'qs','qs','qs',1,'qs','qs');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_articulus`
--

DROP TABLE IF EXISTS `supplier_articulus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_articulus` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned DEFAULT NULL COMMENT 'Продукт (fk product)',
  `supplier_id` int(10) unsigned NOT NULL COMMENT 'Поставщик (fk supplier)',
  PRIMARY KEY (`id`),
  KEY `supplier_articulus_product_FK` (`product_id`),
  KEY `supplier_articulus_supplier_FK` (`supplier_id`),
  CONSTRAINT `supplier_articulus_product_FK` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `supplier_articulus_supplier_FK` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_articulus`
--

LOCK TABLES `supplier_articulus` WRITE;
/*!40000 ALTER TABLE `supplier_articulus` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier_articulus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Email',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Пароль пользователя',
  `admin_id` int(10) unsigned DEFAULT NULL,
  `client_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  KEY `user_admin_FK` (`admin_id`),
  KEY `user_client_FK` (`client_id`),
  CONSTRAINT `FK_8D93D64919EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `FK_8D93D649642B8210` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'qwaso@qip.ru','$2y$13$ui46MjGX1mWeYCh6M004Ze4u/9c6AZCUpItEpLYLUPqJIc.1UJrrO',1,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-18 16:35:39
