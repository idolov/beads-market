<?php

namespace Idolov\CommonConfigBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Бандл для подключения фалов настроек из общей директории для нескольких приложений
 */
class IdolovCommonConfigBundle extends Bundle
{

}
