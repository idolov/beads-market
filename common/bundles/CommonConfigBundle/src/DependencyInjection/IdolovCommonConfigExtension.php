<?php

namespace Idolov\CommonConfigBundle\DependencyInjection;

use Symfony\Component\Config\Exception\FileLocatorFileNotFoundException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Расширение позволяет указывать общие настройки для нескольких приложений
 */
class IdolovCommonConfigExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $this->loadConfig($container, $this->getConfig($configs));
    }

    /**
     * Загружает общие конфигурационные файлы
     * @param ContainerBuilder $container
     * @param array $config
     */
    private function loadConfig($container, $config)
    {
        $configDir = $config[Configuration::NODE_COMMON_CONFIG_DIR]
            ?? $container->getParameter('kernel.project_dir') . '/' . Configuration::DEFAULT_CONFIG_DIR;

        $loader = new Loader\YamlFileLoader(
            $container,
            new FileLocator ($configDir)
        );
        
        $loader->load('services.yaml');

        try {
            $baseConfigfile = Configuration::NODE_ROOT . '.yaml';
            $loader->load($baseConfigfile);
        } catch (FileLocatorFileNotFoundException $e) {

        }

        $configFiles = $config[Configuration::NODE_COMMON_CONFIG_FILENAMES] ?? [];
        $configFiles = array_unique(array_merge($configFiles, $this->getconfigFilenames($container)));


        if ($configFiles) {
            foreach ($configFiles as $file) {
                $loader->load("$file.yaml");
            }
        }
    }

    /**
     * Получает названия конфигурационных файлов, которые должны быть загружены
     * @param ContainerBuilder $container
     * @return array
     */
    private function getconfigFilenames(ContainerBuilder $container)
    {
        $filenamesParam = implode('.', [
            Configuration::NODE_ROOT,
            Configuration::NODE_COMMON_CONFIG_FILENAMES
        ]);

        return $container->hasParameter($filenamesParam)
            ? $container->getParameter($filenamesParam)
            : [];
    }

    /**
     * Получает конфигурацию бандла
     * @param array $configs
     * @return array
     */
    private function getConfig(array $configs)
    {
        $configuration = new Configuration();
        return $this->processConfiguration($configuration, $configs);
    }
}
