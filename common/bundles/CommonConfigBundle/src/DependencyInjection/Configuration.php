<?php

namespace Idolov\CommonConfigBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\NodeInterface;

class Configuration implements ConfigurationInterface
{
    const NODE_ROOT = 'idolov_common_config';
    const NODE_COMMON_CONFIG_DIR = 'config_dir';
    const NODE_COMMON_CONFIG_FILENAMES = 'config_filenames';
    const DEFAULT_CONFIG_DIR = 'common/config';

    /**
     * Generates the configuration tree.
     *
     * @return NodeInterface
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder(self::NODE_ROOT);
        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode(self::NODE_COMMON_CONFIG_DIR)->defaultNull()->end()
                ->arrayNode(self::NODE_COMMON_CONFIG_FILENAMES)
                        ->scalarPrototype()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
