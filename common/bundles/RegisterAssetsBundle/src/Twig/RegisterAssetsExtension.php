<?php

namespace Idolov\RegisterAssetsBundle\Twig;

use Idolov\RegisterAssetsBundle\Twig\TokenParser\InsertScriptsTokenParser;
use Idolov\RegisterAssetsBundle\Twig\TokenParser\InsertStylesheetsTokenParser;
use Idolov\RegisterAssetsBundle\Twig\TokenParser\RegisterCssTokenParser;
use Idolov\RegisterAssetsBundle\Twig\TokenParser\RegisterJsTokenParser;
use Symfony\Component\Asset\Packages;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Twig extension for register stylesheets and scripts on templates and paste on layout template.
 */
class RegisterAssetsExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface Container interface.
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     * @param Packages $packages
     * @throws \Exception
     */
    public function __construct(ContainerInterface $container, Packages $packages)
    {
        $this->container = $container;
        AssetsPathsStore::createInstance($packages);
    }

    /**
     * Returns the token parser instances to add to the existing list.
     * @return array|\Twig_TokenParserInterface[]
     */
    public function getTokenParsers()
    {
        return [
            new InsertScriptsTokenParser(),
            new InsertStylesheetsTokenParser(),
            new RegisterJsTokenParser(),
            new RegisterCssTokenParser(),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'register_assets_extension';
    }
}

