<?php

namespace Idolov\RegisterAssetsBundle\Twig\TokenParser;

use Idolov\RegisterAssetsBundle\Twig\Node\RegisterAssetNode;
use Twig_Error_Syntax;
use Twig_Node;
use Twig_Token;

/**
 * Token parser for tag "register_js"
 */
class RegisterJsTokenParser extends \Twig_TokenParser
{
    /**
     * Parses a token and returns a node.
     * @return Twig_Node A Twig_Node instance
     * @throws Twig_Error_Syntax
     */
    public function parse(Twig_Token $token)
    {
        $parser = $this->parser;
        $stream = $parser->getStream();
        $path = $stream->expect(Twig_Token::STRING_TYPE)->getValue();
        $position = $stream->expect(Twig_Token::NAME_TYPE)->getValue();
        $stream->expect(Twig_Token::BLOCK_END_TYPE);

        return new RegisterAssetNode ($path, $token->getLine(), $this->getTag(), $position);
    }

    /**
     * Gets the tag name associated with this token parser.
     * @return string The tag name
     */
    public function getTag()
    {
        return RegisterAssetNode::TAG_REGISTER_JS;
    }
}
