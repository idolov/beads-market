<?php

namespace Idolov\RegisterAssetsBundle\Twig\TokenParser;

use Idolov\RegisterAssetsBundle\Twig\Node\InsertAssetsNode;
use Twig_Error_Syntax;
use Twig_Node;
use Twig_Token;

/**
 * Token parser for tag "insert_stylesheets"
 */
class InsertStylesheetsTokenParser extends \Twig_TokenParser
{
    /**
     * Parses a token and returns a node.
     * @return Twig_Node A Twig_Node instance
     * @throws Twig_Error_Syntax
     */
    public function parse(Twig_Token $token)
    {
        $parser = $this->parser;
        $stream = $parser->getStream();
        $stream->expect(Twig_Token::BLOCK_END_TYPE);

        return new InsertAssetsNode ($token->getLine(), $this->getTag());
    }

    /**
     * Gets the tag name associated with this token parser.
     * @return string The tag name
     */
    public function getTag()
    {
        return InsertAssetsNode::TAG_INSERT_STYLESHEETS;
    }
}
