<?php

namespace Idolov\RegisterAssetsBundle\Twig;

use Symfony\Component\Asset\Packages;

/**
 * Temporary store of registered assets paths.
 */
class AssetsPathsStore
{
    /** The script is rendered in the head section right before the title element. */
    const POS_HEAD = 'head';

    /** The script is rendered at the beginning of the body section. */
    const POS_BODY_BEGIN = 'body_begin';

    /** The script is rendered at the end of the body section. */
    const POS_BODY_END = 'body_end';

    /** @var self Instance of this class. */
    private static $_instance;

    /** @var Packages Helps manage asset URLs. */
    private $packages;

    /** @var array Positions available for inserting scripts. */
    private $availableScriptsPositions = [
        self::POS_HEAD,
        self::POS_BODY_BEGIN,
        self::POS_BODY_END,
    ];

    /** @var array Paths of JavaScript files. */
    private $scriptsPaths = [];

    /** @var array Paths of stylesheets files. */
    private $stylesheetsPaths = [];

    /**
     * AssetsPathsStore constructor.
     * @param Packages $packages
     */
    private function __construct(Packages $packages)
    {
        $this->packages = $packages;
    }

    /**
     * Creates an instance of a class just once.
     * @param Packages $packages
     * @throws \Exception
     */
    public static function createInstance(Packages $packages)
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self($packages);
            return self::$_instance;
        }

        throw new \Exception('Class instance already created, use "getInstance()."');
    }

    /**
     * Gets instans of assets paths store.
     * @return self
     * @throws \Exception
     */
    public static function getInstance()
    {
        if (!self::$_instance instanceof self) {
            throw new \Exception('Instance not created, use "createInstance()."');
        }

        return self::$_instance;
    }

    /**
     * Adds a script path to the store.
     * @param string $path Script path
     * @param string|null $position Script position to display on page
     * @throws \Exception
     */
    public function addScriptPath($path, $position = null)
    {
        $this->validatePositionToInsertScripts($position);

        if (!isset($this->scriptsPaths[$position])) {
            $this->scriptsPaths[$position] = [];
        }

        if (!in_array($path, $this->scriptsPaths[$position])) {
            $this->scriptsPaths[$position][] = $path;
        }
    }

    /**
     * Displays scripts on page
     * @param string $position Scripts position to display on page
     * @throws \Exception
     */
    public function printScripts($position)
    {
        $this->validatePositionToInsertScripts($position);

        if (!isset($this->scriptsPaths[$position])) {
            return;
        }

        $scriptsHtml = "\n<!-- Begin. These scripts are inserted using RegisterAssetsExtension. -->\n";

        foreach ($this->scriptsPaths[$position] as $scriptPath) {
            $scriptUrl = $this->packages->getUrl($scriptPath);
            $scriptsHtml .= "<script type=\"text/javascript\" src=\"{$scriptUrl}\"></script>\n";
        }

        $scriptsHtml .= "\n<!-- End. These scripts are inserted using RegisterAssetsExtension. -->\n";

        echo $scriptsHtml;
    }

    /**
     * Adds a stylesheet path to the store.
     * @param string $path Stylesheet path
     * @throws \Exception
     */
    public function addStylesheetPath($path)
    {
        if (!in_array($path, $this->stylesheetsPaths)) {
            $this->stylesheetsPaths[] = $path;
        }
    }

    /**
     * Displays stylesheets on page
     * @throws \Exception
     */
    public function printStylesheets()
    {
        if (!count($this->stylesheetsPaths)) {
            return;
        }

        $stylesheetHtml = "\n<!-- Begin. These stylesheets are inserted using RegisterAssetsExtension. -->\n";

        foreach ($this->stylesheetsPaths as $stylesheetPath) {
            $stylesheetUrl = $this->packages->getUrl($stylesheetPath);
            $stylesheetHtml .= "<link rel=\"stylesheet\" href=\"{$stylesheetUrl}\"/>\n";
        }

        $stylesheetHtml .= "\n<!-- End. These stylesheets are inserted using RegisterAssetsExtension. -->\n";

        echo $stylesheetHtml;
    }

    /**
     * Check if the position is available for pasting scripts.
     * @throws \Exception
     */
    private function validatePositionToInsertScripts(string $positionName)
    {
        if (!in_array($positionName, $this->availableScriptsPositions)) {
            throw new \Exception("Position {$positionName} is not available for pasting scripts.
            Specify one of the options: " . implode(', ', $this->availableScriptsPositions) . '.');
        }
    }
}
