<?php

namespace Idolov\RegisterAssetsBundle\Twig\Node;

use Twig_Compiler;

/**
 * Twig node fo tag "insert_scripts" and "insert_stylesheets"
 */
class InsertAssetsNode extends \Twig_Node
{
    const ATTRIBUTE_POSITION = 'position';
    const TAG_INSERT_SCRIPTS = 'insert_scripts';
    const TAG_INSERT_STYLESHEETS = 'insert_stylesheets';

    /**
     * InsertAssetsNode constructor.
     * @param int $line
     * @param string $tag
     * @param string|null $position
     */
    public function __construct($line, $tag, $position = null)
    {
        parent:: __construct([], [self::ATTRIBUTE_POSITION => $position], $line, $tag);
    }

    /**
     * @param Twig_Compiler $compiler
     * @throws \Exception
     */
    public function compile(Twig_Compiler $compiler)
    {
        if ($this->tag == self::TAG_INSERT_SCRIPTS) {
            $this->compileRegisterJs($compiler);
        } elseif ($this->tag == self::TAG_INSERT_STYLESHEETS) {
            $this->compileRegisterCss($compiler);
        } else {
            throw new \Exception("Tag \"{$this->tag}\" is not available.");
        }
    }

    /**
     * @param Twig_Compiler $compiler
     */
    private function compileRegisterJs(Twig_Compiler $compiler)
    {
        $compiler
            ->addDebugInfo($this)
            ->write("Idolov\\RegisterAssetsBundle\\Twig\\AssetsPathsStore::getInstance()
                ->printScripts('{$this->getAttribute(self::ATTRIBUTE_POSITION)}')")
            ->raw("; \n ");
    }

    /**
     * @param Twig_Compiler $compiler
     */
    private function compileRegisterCss(Twig_Compiler $compiler)
    {
        $compiler
            ->addDebugInfo($this)
            ->write("Idolov\\RegisterAssetsBundle\\Twig\\AssetsPathsStore::getInstance()
                ->printStylesheets()")
            ->raw("; \n ");
    }
}
