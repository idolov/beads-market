<?php

namespace Idolov\RegisterAssetsBundle\Twig\Node;

use Twig_Compiler;

/**
 * Twig node fo tag "register_js"
 */
class RegisterAssetNode extends \Twig_Node
{
    const ATTRIBUTE_PATH = 'path';
    const ATTRIBUTE_POSITION = 'position';
    const TAG_REGISTER_JS = 'register_js';
    const TAG_REGISTER_CSS = 'register_css';

    /**
     * RegisterAssetNode constructor.
     * @param string $path Asset path
     * @param int $line
     * @param string $tag
     * @param string|null $position Scripts position to display on page
     */
    public function __construct($path, $line, $tag, $position = null)
    {
        $this->tag = $tag;

        parent:: __construct([], [
            self::ATTRIBUTE_PATH => $path,
            self::ATTRIBUTE_POSITION => $position
        ], $line, $tag);
    }

    /**
     * @param Twig_Compiler $compiler
     * @throws \Exception
     */
    public function compile(Twig_Compiler $compiler)
    {
        if ($this->tag == self::TAG_REGISTER_JS) {
            $this->compileRegisterJs($compiler);
        } elseif ($this->tag == self::TAG_REGISTER_CSS) {
            $this->compileRegisterCss($compiler);
        } else {
            throw new \Exception("Tag \"{$this->tag}\" is not available.");
        }
    }

    /**
     * @param Twig_Compiler $compiler
     */
    private function compileRegisterJs(Twig_Compiler $compiler)
    {
        $compiler
            ->addDebugInfo($this)
            ->write("Idolov\\RegisterAssetsBundle\\Twig\\AssetsPathsStore::getInstance()
                ->addScriptPath(
                    '{$this->getAttribute(self::ATTRIBUTE_PATH)}',
                    '{$this->getAttribute(self::ATTRIBUTE_POSITION)}'
                )")
            ->raw("; \n ");
    }

    /**
     * @param Twig_Compiler $compiler
     */
    private function compileRegisterCss(Twig_Compiler $compiler)
    {
        $compiler
            ->addDebugInfo($this)
            ->write("Idolov\\RegisterAssetsBundle\\Twig\\AssetsPathsStore::getInstance()
                ->addStylesheetPath(
                    '{$this->getAttribute(self::ATTRIBUTE_PATH)}'
                )")
            ->raw("; \n ");
    }
}
