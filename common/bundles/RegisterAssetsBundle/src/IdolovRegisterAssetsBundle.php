<?php

namespace Idolov\RegisterAssetsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle for registration of assets and pasting in layout.
 */
class IdolovRegisterAssetsBundle extends Bundle
{

}
