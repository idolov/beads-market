# register-assets-bundle

install
```
$ composer require idolov/register-assets-bundle
```

```php
<?php
//config/bundles.php
return [
    Idolov\RegisterAssetsBundle\IdolovRegisterAssetsBundle::class => ['all' => true],
];
```

Adds a stylesheet path to the store (in template).
```twig
{% register_css 'build/css/style.css' %}
```

Adds a JavaScript file path to the store (in template).
The script is rendered in the head section right before the title element.
```twig
{% register_js 'build/js/script.js' head %}
```

The script is rendered at the beginning of the body section.
```twig
{% register_js 'build/js/script.js' body_begin %}
```

The script is rendered at the end of the body section. (in layout)
```twig
{% register_js 'build/js/script.js' body_end %}
```

Displays stylesheets on page. (in layout)
```twig
{% insert_stylesheets %}
```

Displays scripts on page.
```twig
{% insert_scripts head %}
```
or
```twig
{% insert_scripts body_begin %}
```
or
```twig
{% insert_scripts body_end %}
```
