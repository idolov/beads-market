# menu-bundle

install
```
$ composer require idolov/menu-bundle
```

```php
<?php 
//config/bundles.php
return [
    Idolov\MenuBundle\IdolovMenuBundle::class => ['all' => true],
];
```


## Меню
Настройка меню
```yaml
idolov_menu:
    menus:
        main_menu: # Название меню
            class: Idolov\MenuBundle\Model\menu\Menu # Класс меню
            items: # Элементы меню
                # Технически, набор полей может быть произвольным, но он должен быть отражен в классе элемента меню, в методе getFieldNames()
                # Если предполагается использовать меню в качестве базиса для отображения хлебных крошек,
                # необходимо  задать поле, которое будет использоваться в качестве ключа и будет возвращаться в методе getKey() (см. MenuItem)
                -   title: Каталог
                    custom_field: custom-value
                    key: catalog_module
                    children: # Дочерние элементы
                        -   title: Товары
                            route: product_index
```

Элемент меню может быть помечен как активный или неактивный и обработан особым образом в шаблоне twig.
Так же, при построении хлебных крошек на основе меню, берется текущий активный элемент меню и от него
строятся хлебные крошки до корневого элемента.
Помечаем текущий активный элемент меню:
```php
<?php 
$container->get('idolov.menu_provider')
    ->getMenu('menu_name')
    ->setActiveItemKey('item_key');
```

Добавление нового меню в php
```php
<?php 
    $menu = MenuProvider::createMenu('test_menu');
    $menu->setItems([
        [
            'key' => 'one',
            'title' => 'First item',
            'children' => [
                [
                    'key' => 'ch',
                    'title' => 'Child'
                ],
            ]
        ],
        [
            'key' => 'two',
            'title' => 'Second item'
        ]
    ]);
    $this->container->get('idolov.menu_provider')->addMenu($menu);
```

Добавление локального меню в контроллере.
Первый способ:
1. В контроллере подключите Idolov\MenuBundle\Controller\MenuInControllerTrait
2. Переопреде метод menuConfigs
3. В экшне запустите инициализацию меню

Второй способ:
1. В контроллере подключите Idolov\MenuBundle\Controller\MenuInControllerTrait
2. В экшне запустите инициализацию меню, и уже в созданное меню добавьте пункты
```php
<?php
    class AdminController extends Controller
    {
        use MenuInControllerTrait;
        protected function menuConfigs(): array
        {
            return [
                'menu_name' => [
                    'title_1' => 'route_1',
                    'title_2' => 'route_2',
                ]
            ];
        }
        
        // Инициализация меню из настроек
        public function actionIndex()
        {
            $this->initMenu('menu_name', ['route_param' => 'param_value']);
        }
        
        // Инициализация меню сразу в экшне
        public function actionIndex2()
        {
            $this->initMenu('menu_name')
                ->addItem([
                    'key' => 'item1',
                    'title' => 'Item title',
                    'url' => $this->generateUrl('route_1', ['route_param' => 'param_value']),
                ])
                ->addItem([
                    'key' => 'item2',
                    'title' => 'Item title',
                    'url' => $this->generateUrl('route_2', ['route_param' => 'param_value']),
                ]);
        }
    }
```

Рендеринг меню в шаблоне (main_menu - название меню)
```twig
{{ insert_menu('main_menu') }}
```

## Хлебные крошки

Настройка хлебных крошек:
```yaml
idolov_menu:
    breadcrumbs:
        class: Idolov\MenuBundle\Model\breadcrumbs\Breadcrumbs # Класс хлебных крошек
```

Добавление хлебных крошек в контроллере:
```php
<?php 
$container->get('idolov.breadcrumbs_provider')
    ->addCrumb(['title' => 'Крошка', 'url' => '/url'])
    ->addCrumb(['title' => 'Крошка 2', 'url' => '/url2']);
```

Добавление хлебных крошек с дочерними элементами:
```php
<?php 
$crumb = new BreadcrumbsItem([
    'title' => 'Крошка',
    'url' => '/url'
]);

// Добавленные дочерние элементы (будут отображаться в выпадающем списке)
$crumb->addChild(
    new BreadcrumbsItem([
        'title' => 'Вторая крошка',
        'route' => 'route_name'
    ])
);

$container->get('idolov.breadcrumbs_provider')->addCrumb($crumb);
```

Следует помнить, что блок, в котором добавляются хлебные крошки, должен быть размещен выше места, где они рендерятся (связано с особенностью работы шаблонизатора twig).
Добавление хлебной крошки в шаблоне:
```twig
{% extends 'layout.html.twig' %}
{% block breadcrumbs %}
    {{ breadcrumb('Заголовок крошки', '/url') }}
 {% endblock %}
```

Рендеринг хлебных крошек в шаблоне:
```twig
{# layout.html.twig #}
{% block breadcrumbs %}{% endblock %}
{{ insert_breadcrumbs() }}
```
