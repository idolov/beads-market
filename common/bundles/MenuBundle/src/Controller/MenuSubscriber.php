<?php

namespace Idolov\MenuBundle\Controller;

use Idolov\MenuBundle\Service\BreadcrumbsProvider;
use Idolov\MenuBundle\Service\MenuProvider;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Подписчик на события системы, для управления меню и хлебными крошками
 */
class MenuSubscriber implements EventSubscriberInterface
{
    /** @var MenuProvider Сервис управления меню */
    private $menuProvider;

    /** @var BreadcrumbsProvider Сервис управления хлебными крошками */
    private $breadcrumbsProvider;

    /**
     * @param MenuProvider $menuProvider Сервис управления меню
     * @param BreadcrumbsProvider $breadcrumbsProvider Сервис управления хлебными крошками
     */
    public function __construct(MenuProvider $menuProvider, BreadcrumbsProvider $breadcrumbsProvider)
    {
        $this->menuProvider = $menuProvider;
        $this->breadcrumbsProvider = $breadcrumbsProvider;
    }

    /**
     * @inheritdoc
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onBeforeAction',
        ];
    }

    /**
     * Обработчик события "onBeforeAction".
     * Действия перед выполнением экшна контроллера.
     * @param ControllerEvent $event
     * @throws \Exception
     */
    public function onBeforeAction(ControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        $controller = array_shift($controller);
        if ($controller instanceof MenuInterface) {

            $mainMenuName = $controller->getMainMenuName();

            $this->provideMainMenuActiveKey(
                $mainMenuName,
                $controller->getMainMenuActiveKey()
            );

            $this->initBreadcrumbsFromMenu($mainMenuName);
        }
    }

    /**
     * Устанавливает ключ актиного элемента в главном меню
     * @param string $menuName Идентификатор меню
     * @param string $activeKey Ключ активного элемента меню
     */
    private function provideMainMenuActiveKey($menuName, $activeKey): void
    {
        if ($menuName && $activeKey) {
            $this->menuProvider->getMenu($menuName)->setActiveItemKey($activeKey);
        }
    }

    /**
     * Инициализирует хлебные крошки на основе менню
     * @param string $menuName Идентификатор меню для генерации хлебных крошек
     * @throws \Exception
     */
    private function initBreadcrumbsFromMenu($menuName)
    {
        if ($menuName && $menu = $this->menuProvider->getMenu($menuName)) {
            $this->breadcrumbsProvider->initCrumbsFromMenu($menu);
        }
    }
}
