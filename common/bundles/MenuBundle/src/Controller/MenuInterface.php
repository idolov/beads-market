<?php

namespace Idolov\MenuBundle\Controller;

/**
 * Интерфейс контроллера для привязки главного меню и управления хлебными крошками
 * @see MenuSubscriber
 *
 */
interface MenuInterface
{
    /**
     * Получает идентификатор главного меню
     * @return null|string
     */
    public function getMainMenuName(): ?string;

    /**
     * Получает ключ активного пункта главного меню, для экшнов контроллера.
     * @return null|string
     */
    public function getMainMenuActiveKey(): ?string;
}
