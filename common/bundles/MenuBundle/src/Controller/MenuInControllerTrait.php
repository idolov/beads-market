<?php

namespace Idolov\MenuBundle\Controller;

use Idolov\MenuBundle\DependencyInjection\Configuration;
use Idolov\MenuBundle\Model\menu\Menu;
use Idolov\MenuBundle\Model\menu\MenuInController;
use Idolov\MenuBundle\Service\MenuProvider;
use Psr\Container\ContainerInterface;

/**
 * Добавляет функции управления различными меню на страницах контроллера
 */
trait MenuInControllerTrait
{
    /**
     * Получает объект сервис-контейнера symfony
     * @return ContainerInterface|null
     */
    abstract protected function getContainer(): ?ContainerInterface;

    /**
     * Получает элементы различных меню, отображаемых на страницах контроллера
     * @return array
     * [
     *    'menu_name' => [
     *        'title_1' => 'route_1',
     *        'title_2' => 'route_2',
     *     ]
     *  ]
     */
    protected function menuConfigs(): array
    {
        return [];
    }

    /**
     * Получает массив элементов меню
     * @param string $menuName Идентификатор меню
     * @param array|null $routeParams Параметры для генерации урлов меню
     * @return array
     * @throws \Exception
     */
    private function getMenuItems(string $menuName, array $routeParams): ?array
    {
        $container = $this->getContainer();
        if (!($container instanceof ContainerInterface)) {
            throw new \Exception('Контейнер еще не инициализирован');
        }

        $menuItems = [];
        foreach ($this->getMenuConfig($menuName) ?? [] as $title => $route) {
            $menuItems[] = [
                'title' => $title,
                'route' => $route,
                'url' => $container->get('router')->generate(
                    $route,
                    $routeParams[$route] ?? $routeParams
                ),
            ];
        }

        return $menuItems;
    }

    /**
     * Инициализирует меню
     * @param string $menuName Идентификатор меню
     * @param array|null $routeParams Параметры для генерации урлов меню
     * @return Menu
     * @throws \Exception
     */
    protected function initMenu(string $menuName, ?array $routeParams = null)
    {
        $container = $this->getContainer();
        if ($container instanceof ContainerInterface) {

            /** @var \Idolov\MenuBundle\Service\MenuProvider $menuProvider */
            $menuProvider = $container->get(Configuration::SERVICE_MENU_PROVIDER);

            if (!$menuProvider->hasMenu($menuName)) {
                $menuProvider->addMenu(
                    MenuProvider::createMenu($menuName, $this->getMenuClassName())
                        ->setItems($this->getMenuItems($menuName, $routeParams ?? []))
                );
            }

            if (($menu = $menuProvider->getMenu($menuName)) && $this->isValidMenuObject($menu)) {
                return $menu;
            };
        }

        throw new \Exception("Меню с идентификатором $menuName не может быть инициализировано");
    }

    /**
     * Проверяет, существуют ли настройки меню с указанным идентификатором
     * @param string $menuName Идентификатор меню
     * @return bool
     */
    private function hasMenuConfig(string $menuName): bool
    {
        $menuConfigs = $this->menuConfigs();
        return isset($menuConfigs[$menuName]);
    }

    /**
     * Получает настройки меню по идентификатору
     * @param string $menuName Идентификатор меню
     * @return array|null
     */
    private function getMenuConfig(string $menuName): ?array
    {
        $menuConfigs = $this->menuConfigs();
        return $menuConfigs[$menuName] ?? null;
    }

    /**
     * Пероверяет, является ли переданный аргумент валидным объектом - меню
     * @param mixed $menu
     * @return bool
     */
    final private function isValidMenuObject($menu): bool
    {
        $menuClass = $this->getMenuClassName();
        return $menu instanceof $menuClass;
    }

    /**
     * Получает класс меню контроллера
     * @return string
     */
    protected function getMenuClassName()
    {
        return MenuInController::class;
    }
}
