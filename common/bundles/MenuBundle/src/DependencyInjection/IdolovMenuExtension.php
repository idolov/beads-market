<?php

namespace Idolov\MenuBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Menu extension.
 */
class IdolovMenuExtension extends Extension
{
    /**
     * Загружает конфигурацию расширения
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader(
            $container,
            new FileLocator (__DIR__ . "/../../config")
        );

        $loader->load("services.yml");
        $this->loadConfig($container, $config);
    }

    /**
     * Загружает конфигурацию в контейнер
     * @param ContainerBuilder $container
     * @param array $config
     */
    private function loadConfig(ContainerBuilder $container, array $config)
    {
        // Set parameter idolov_menu.menus
        $container->setParameter(
            implode('.', [
                Configuration::NODE_ROOT,
                Configuration::NODE_MENUS
            ]),
            $config[Configuration::NODE_MENUS]
        );

        // Set parameter idolov_menu.breadcrumbs
        $container->setParameter(
            implode('.', [
                Configuration::NODE_ROOT,
                Configuration::NODE_BREADCRUMBS
            ]),
            $config[Configuration::NODE_BREADCRUMBS]
        );
    }
}
