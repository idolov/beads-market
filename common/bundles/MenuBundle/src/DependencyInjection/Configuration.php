<?php

namespace Idolov\MenuBundle\DependencyInjection;

use Idolov\MenuBundle\Model\breadcrumbs\Breadcrumbs;
use Idolov\MenuBundle\Model\menu\Menu;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\NodeInterface;

class Configuration implements ConfigurationInterface
{
    const SERVICE_MENU_PROVIDER = 'idolov.menu_provider';

    /** Config root node */
    const NODE_ROOT = 'idolov_menu';

    /** В настройках можно указать несколько меню, ключ в этом массиве, является идентификатором меню */
    const NODE_MENUS = 'menus';

    /** Список элементов меню */
    const NODE_MENU_ITEMS = 'items';

    /** Класс меню или хлебных крошек */
    const NODE_CLASS_NAME = 'class';

    /** Потомки элемента меню (подменю) */
    const NODE_CHILDREN = 'children';

    /** Шаблон рендеринга (Twig template) */
    const NODE_TEMPLATE_NAME = 'template';

    /** Настройки хлебных крошек */
    const NODE_BREADCRUMBS = 'breadcrumbs';

    /** Шаблон рендеринга меню по умолчанию */
    const DEFAULT_MENU_TEMPLATE = '@IdolovMenu/menu.html.twig';

    /** Шаблон рендеринга меню контроллера по умолчанию */
    const DEFAULT_MENU_IN_CONTROLLER_TEMPLATE = '@IdolovMenu/menu_in_controller.html.twig';

    /** Шаблон рендеринга хлебных крошек по умолчанию */
    const DEFAULT_BREADCRUMBS_TEMPLATE = '@IdolovMenu/breadcrumbs.html.twig';

    /**
     * Generates the configuration tree.
     *
     * @return NodeInterface
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root(self::NODE_ROOT);
        $rootNode->children()
            ->arrayNode(self::NODE_MENUS)
            ->arrayPrototype()
            ->children()
                ->scalarNode(self::NODE_CLASS_NAME)->defaultValue(Menu::class)->end()
                ->arrayNode(self::NODE_MENU_ITEMS)
                    ->arrayPrototype()
                    ->children()
                        ->arrayNode(self::NODE_CHILDREN)
                            ->arrayPrototype()
                                ->scalarPrototype()->end()
                            ->end()
                        ->end()
                    ->end()
                    ->prototype('variable')->end()
                ->end()
                ->end()
            ->end()
            ->end()
            ->end() // self::NODE_MENUS
            ->arrayNode(self::NODE_BREADCRUMBS)
                ->children()
                    ->scalarNode(self::NODE_CLASS_NAME)->defaultValue(Breadcrumbs::class)->end()
                ->end()
                ->end() // self::NODE_BREADCRUMBS
        ->end();

        return $treeBuilder;
    }
}
