<?php

namespace Idolov\MenuBundle\Service;

use Idolov\MenuBundle\DependencyInjection\Configuration;
use Idolov\MenuBundle\Model\base\BaseTreeItem;
use Idolov\MenuBundle\Model\breadcrumbs\Breadcrumbs;
use Idolov\MenuBundle\Model\breadcrumbs\BreadcrumbsItem;
use Idolov\MenuBundle\Model\menu\Menu;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Сервис для управления хлебными крошками
 */
class BreadcrumbsProvider
{
    /** @var Breadcrumbs Структура данных, элементы которой и являются хлебными крошками */
    private $breadcrumbs;

    /**
     * Конструктор сервиса хлебных крошек
     * @param ContainerInterface $container
     * @throws \Exception
     */
    public function __construct(ContainerInterface $container)
    {
        // Get parameter idolov_menu.breadcrumbs
        $params = $container->getParameter(implode('.', [
            Configuration::NODE_ROOT,
            Configuration::NODE_BREADCRUMBS
        ]));

        $this->initBreadcrumbs($params[Configuration::NODE_CLASS_NAME]);
    }

    /**
     * Получает объект хлебных крошек
     * @return Breadcrumbs
     */
    public function getBreadcrumbs(): Breadcrumbs
    {
        return $this->breadcrumbs;
    }

    /**
     * Получает имя шаблона для рендеринга хлебных крошек
     * @return string
     */
    public function getTemplateName()
    {
        return $this->breadcrumbs->getTemplateName();
    }

    /**
     * Добавляет хлебную крошку
     * @param BreadcrumbsItem|array $item Массив с параметрами или сам объект - хлебная крошка
     * @throws \Exception
     */
    public function addCrumb($item)
    {
        $this->breadcrumbs->addItem($item);

        return $this;
    }

    /**
     * Создает объект хлебной крошки
     * @param array $itemParams Массив с параметрами хлебной крошки
     * @return BreadcrumbsItem
     * @throws \Exception
     */
    public function createCrumb(array $itemParams): BreadcrumbsItem
    {
        /** @var BreadcrumbsItem $crumb */
        $crumb = $this->breadcrumbs->createItem($itemParams);
        return $crumb;
    }

    /**
     * Инициализирует объект, который будет хранить хлебные крошки
     * @param string $className Имя класса хлебных крошек
     * @throws \Exception
     */
    private function initBreadcrumbs(string $className)
    {
        if (!(is_subclass_of($className, Breadcrumbs::class) || ($className === Breadcrumbs::class))) {
            throw new \Exception('The parameter $className must be a descendant of '
                . Breadcrumbs::class);
        }

        $this->breadcrumbs = new $className();
    }

    /**
     * Строит хлебные крошки на основе уже готового меню.
     * Хлебные крошки строятся от текущего активного элемента меню, к корневому,
     * путем последовательного перебора всех родителских элементов по цепочке, если такие имеются.
     * @param Menu $menu Меню, на основе которого производится построение хлебных крошек
     * @throws \Exception
     */
    public function initCrumbsFromMenu(Menu $menu)
    {
        if (!$menu->hasActiveItem()) {
            return;
        }

        $breadcrumbs = [];
        $menuItem = $menu->getActiveItem();
        $prevMenuItem = null;
        do {
            if ($menuItem) {
                array_unshift($breadcrumbs, $this->createCrumbFromMenuItem($menuItem, $prevMenuItem));
            }
            $prevMenuItem = $menuItem;
            $menuItem = $menuItem->getParent();
        } while ($menuItem);

        $this->breadcrumbs->setItems($breadcrumbs);
    }

    /**
     * Создает объект хлебных крошек на основе элемента какого либо меню
     * @param BaseTreeItem $menuItem Элемент меню
     * @param BaseTreeItem|null $prevMenuItem
     * @return BaseTreeItem
     * @throws \Exception
     */
    private function createCrumbFromMenuItem(BaseTreeItem $menuItem, BaseTreeItem $prevMenuItem = null): BaseTreeItem
    {
        $crumb = $this->createCrumb($menuItem->getAttributes());
        if ($menuItem->hasChildren()) {
            $prevItemKey = $prevMenuItem ? $prevMenuItem->getKey() : null;
            foreach ($menuItem->getChildren() as $child) {

                /* Проверяем если дочерний элемент уже добавлен в хлебные крошки, пропускаем его, чтобы не выводить два
                раза один и тот же элемент и в хлебных крошках и как дочерний элемент родительской хлебной крошки */
                if ($prevItemKey && ($prevItemKey === $child->getKey())) {
                    continue;
                }

                $crumb->addChild(
                    $this->createCrumb(
                        $child->getAttributes()
                    )
                );
            }
        }

        return $crumb;
    }
}

