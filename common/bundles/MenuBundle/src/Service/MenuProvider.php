<?php

namespace Idolov\MenuBundle\Service;

use Idolov\MenuBundle\DependencyInjection\Configuration;
use Idolov\MenuBundle\Model\menu\Menu;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Сервис для управления различными меню
 */
class MenuProvider
{
    /** @var Menu[] Коллекция объектов различных меню */
    private $menus = [];

    /**
     * Конструктор сервиса.
     * Создет меню на основе заданных параметров конфигурации
     * @param ContainerInterface $container
     * @throws \Exception
     */
    public function __construct(ContainerInterface $container)
    {
        // Get parameter idolov_menu.menus
        $menuConfig = $container->getParameter(Configuration::NODE_ROOT . '.' . Configuration::NODE_MENUS);

        if (count($menuConfig)) {
            foreach ($menuConfig as $menuName => $menuParams) {
                $this->addMenu(
                    static::createMenu($menuName, $menuParams[Configuration::NODE_CLASS_NAME])
                        ->setItems($menuParams[Configuration::NODE_MENU_ITEMS] ?? [])
                );
            }
        }
    }

    /**
     * Проверяет, существует ли объект меню с указанным идентификатором
     * @param string $menuName Идентификатор меню
     * @return bool
     */
    public function hasMenu($menuName)
    {
        return isset($this->menus[$menuName])
            && ($this->menus[$menuName] instanceof Menu);
    }

    /**
     * Получает объект меню по идентификатору
     * @param string $menuName Идентификатор меню
     * @return Menu
     */
    public function getMenu($menuName)
    {
        return $this->menus[$menuName] ?? null;
    }

    /**
     * Добавляет объект меню в коллекцию
     * @param Menu $menu Объект меню
     * @throws \Exception
     */
    public function addMenu(Menu $menu)
    {
        $this->menus[$menu->getName()] = $menu;
    }

    /**
     * Создает объект меню
     * @param string $menuName Идентификатор меню
     * @param string $menuClass Имя класса меню
     * @return Menu
     * @throws \Exception
     */
    public static function createMenu(string $menuName, string $menuClass): Menu
    {
        if (!(is_subclass_of($menuClass, Menu::class) || ($menuClass === Menu::class))) {
            throw new \Exception('The parameter $menuClass must be a descendant of '
                . Menu::class);
        }

        return new $menuClass($menuName);
    }
}
