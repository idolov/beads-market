<?php

namespace Idolov\MenuBundle\Model\menu;

use Idolov\MenuBundle\DependencyInjection\Configuration;

/**
 * Меню контроллера
 */
class MenuInController extends Menu
{
    /**
     * @inheritdoc
     */
    public function getTemplateName(): string
    {
        return Configuration::DEFAULT_MENU_IN_CONTROLLER_TEMPLATE;
    }

    /**
     * @inheritdoc
     */
    protected function getItemClass(): string
    {
        return MenuInControllerItem::class;
    }
}

