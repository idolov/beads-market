<?php

namespace Idolov\MenuBundle\Model\menu;

use Idolov\MenuBundle\DependencyInjection\Configuration;
use Idolov\MenuBundle\Model\base\BaseTree;
use Idolov\MenuBundle\Model\base\BaseTreeItem;

/**
 * Меню
 */
class Menu extends BaseTree
{
    /** @var string Идентификатор меню */
    private $menuName;

    /** @var string Ключ активного элемента меню */
    private $activeItemKey;

    /**
     * Menu constructor
     * @param string $menuName Идентификатор меню
     * @throws \Exception
     */
    public function __construct(string $menuName) {
        $itemClass = $this->getItemClass();
        if (!(is_subclass_of($itemClass, MenuItem::class) || ($itemClass === MenuItem::class))) {
            throw new \Exception('The parameter $itemClass must be a descendant of '
                . MenuItem::class);
        }
        $this->menuName = $menuName;
        parent::__construct();
    }

    /**
     * Получает идентификатор меню
     * @return string
     */
    public function getName(): string
    {
        return $this->menuName;
    }

    /**
     * @inheritdoc
     */
    public function getTemplateName(): string
    {
        return Configuration::DEFAULT_MENU_TEMPLATE;
    }

    /**
     * Проверяет задан ли активный элемент меню
     * @return bool
     */
    public function hasActiveItem()
    {
        return $this->activeItemKey && $this->hasItem($this->activeItemKey);
    }

    /**
     * Получает активный элемент меню
     * @return BaseTreeItem|MenuItem|null
     */
    public function getActiveItem()
    {
        return $this->hasActiveItem()
            ? $this->getItem($this->activeItemKey)
            : null;
    }

    /**
     * Получает ключ активного элемента меню
     * @return string
     */
    public function getActiveItemKey()
    {
        return $this->activeItemKey;
    }

    /**
     * Задает ключ активного элемента меню
     * @param string $key Ключ элемента меню
     */
    public function setActiveItemKey(string $key)
    {
        $this->activeItemKey = $key;
    }

    /**
     * Помечает элемент меню как активный, если он таким является
     * @param MenuItem $item Элемент меню
     * @throws \Exception
     */
    private function markItemAsActive(MenuItem $item)
    {
        if ($this->activeItemKey
            && ($item->getKey() === $this->activeItemKey)
        ) {
            $item->markAsActive();
        } else {
            $item->markAsInactive();
        }
    }

    /**
     * Конвертирует элемент меню в массив
     * @param MenuItem $item Элемент меню
     * @return array
     * @throws \Exception
     */
    protected function itemToArray(BaseTreeItem $item): array
    {
        $menu = $this;
        return $item->toArray(function ($self) use($menu) {
            $menu->markItemAsActive($self);
        });
    }

    /**
     * @inheritdoc
     */
    protected function getItemClass(): string
    {
        return MenuItem::class;
    }
}
