<?php

namespace Idolov\MenuBundle\Model\menu;

use Idolov\MenuBundle\Model\base\BaseTreeItem;

/**
 * Элемент меню
 * @property string $key Ключ текущего элемента
 * @property string $title Заголовок элемента меню
 * @property string $route Маршрут, для генерции URL
 */
class MenuItem extends BaseTreeItem
{
    /** @var bool Признак, что элемент является активным */
    private $isActive = false;

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function getKey(): string
    {
        if (isset($this->key)) {
            return $this->key;
        }

        if (is_string($this->route)) {
            return $this->route;
        }

        throw new \Exception('Fill the property $key or $route');
    }

    /**
     * Помечает текущий элемент как активный
     * @return void
     */
    public function markAsActive()
    {
        $this->isActive = true;
    }

    /**
     * Помечает текущий элемент как неактивный
     * @return void
     */
    public function markAsInactive()
    {
        $this->isActive = false;
    }

    /**
     * Проверяет, является ли данный элемент активным
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @inheritdoc
     */
    public function toArray(callable $actionBefore = null)
    {
        $self = parent::toArray($actionBefore);

        if ($this->isActive()) {
            $self['active'] = true;
        }

        return $self;
    }

    /**
     * @inheritdoc
     */
    protected function getFieldNames()
    {
        return [
            'key',
            'title',
            'route',
        ];
    }
}
