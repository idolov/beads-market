<?php

namespace Idolov\MenuBundle\Model\menu;

/**
 * Элемент меню контроллера
 */
class MenuInControllerItem extends MenuItem
{
    /**
     * @inheritdoc
     */
    protected function getFieldNames()
    {
        return array_merge([
            'url',
        ], parent::getFieldNames());
    }
}
