<?php

namespace Idolov\MenuBundle\Model\breadcrumbs;

use Idolov\MenuBundle\DependencyInjection\Configuration;
use Idolov\MenuBundle\Model\base\BaseTree;

/**
 * Хлебные крошки
 */
class Breadcrumbs extends BaseTree
{
    /**
     * Breadcrumbs constructor
     * @throws \Exception
     */
    public function __construct()
    {
        $itemClass = $this->getItemClass();
        if (!(is_subclass_of($itemClass, BreadcrumbsItem::class) || ($itemClass === BreadcrumbsItem::class))) {
            throw new \Exception('The parameter $itemClass must be a descendant of '
                . BreadcrumbsItem::class);
        }

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function getItemClass(): string
    {
        return BreadcrumbsItem::class;
    }

    /**
     * @inheritdoc
     */
    public function getTemplateName(): string
    {
        return Configuration::DEFAULT_BREADCRUMBS_TEMPLATE;
    }
}
