<?php

namespace Idolov\MenuBundle\Model\breadcrumbs;

use Idolov\MenuBundle\Model\base\BaseTreeItem;

/**
 * Элемент хлебных крошек
 * @property string $title Заголовок хлебной крошки
 * @property string $route Маршрут, для генерции URL
 * @property string $url Уже сгенерированный урл
 */
class BreadcrumbsItem extends BaseTreeItem
{
    /**
     * Получает список свойств данного объекта
     * @return array
     */
    protected function getFieldNames()
    {
        return [
            'title',
            'route',
            'url',
        ];
    }
}
