<?php

namespace Idolov\MenuBundle\Model\base;

use Idolov\MenuBundle\DependencyInjection\Configuration;

/**
 * Абстрактный класс структуры данных
 */
abstract class BaseTree
{
    /** @var array Элементы структуры */
    private $items = [];

    /** @var array Сквозной список элементов структуры, без иерархического разделения */
    private $itemsIndex = [];

    /**
     * Получает имя класса элемента сруктуры
     * @return string
     */
    abstract protected function getItemClass(): string;

    /**
     * Получает имя шаблона для рендеринга структуры
     * @return string
     */
    abstract protected function getTemplateName(): string;

    /**
     * Tree constructor
     * @throws \Exception
     */
    public function __construct()
    {
        $itemClass = $this->getItemClass();
        if (!is_subclass_of($itemClass, BaseTreeItem::class)) {
            throw new \Exception('The parameter $itemClass must be a descendant of '
                . BaseTreeItem::class);
        }
    }

    /**
     * Получает элементы структуры
     * @return BaseTreeItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Заполняет элементы структуры
     * @param array $items Массив элементов структуры
     * (Массив объектов или просто многомерный массив с параметрами элементов структуры)
     * @return $this
     * @throws \Exception
     */
    public function setItems(array $items): self
    {
        foreach ($items as $item) {
            $this->addItem($item);
        }

        return $this;
    }

    /**
     * Добавляет элемент структуры
     * @param BaseTreeItem|array $item Элемент структуры
     * @return $this
     * @throws \Exception
     */
    public function addItem($item): self
    {
        if (is_array($item)) {
            $item = $this->createItemWithChildren($item);
        }

        if ($this->isValidItemObject($item)) {
            $this->addItemToIndex($item);
            $this->items[] = $item;
            return $this;
        }

        throw new \Exception('Nothing to add');
    }

    /**
     * Получает элемент по ключу
     * @param string $key Ключ элемента структуры
     * @return BaseTreeItem|null
     */
    public function getItem($key)
    {
        return $this->itemsIndex[$key] ?? null;
    }

    /**
     * Проверяет существует ли элемент структуры с ключем
     * @param string $key Ключ элемента структуры
     * @return bool
     */
    public function hasItem($key)
    {
        return isset($this->itemsIndex[$key]);
    }

    /**
     * Конвертирует структуру в многомерный массив
     * @return array
     */
    public function toArray(): array
    {
        $result = [];
        /** @var BaseTreeItem $item */
        foreach ($this->getItems() as $item) {
            $result[] = $this->itemToArray($item);
        }

        return $result;
    }

    /**
     * Создает элемент структуры
     * @param array $itemParams Параметры элемента структуры
     * @return BaseTreeItem
     * @throws \Exception
     */
    public function createItem($itemParams)
    {
        $itemClass = $this->getItemClass();
        return new $itemClass($itemParams);
    }

    /**
     * Рекурсивно создает элемент структуры из массива, включая дочерние элементы
     * @param array $itemParams Параметры элемента структуры
     * @return BaseTreeItem
     * @throws \Exception
     */
    public function createItemWithChildren($itemParams)
    {
        $item = $this->createItem($itemParams);
        if (isset($itemParams[Configuration::NODE_CHILDREN])) {
            foreach ($itemParams[Configuration::NODE_CHILDREN] as $childParams) {
                $child = $this->createItemWithChildren($childParams);
                $item->addChild($child);
            }
        }

        return $item;
    }

    /**
     * Конвертирует элемент структуры в массив
     * @param BaseTreeItem $item Элемент структуры
     * @return array
     */
    protected function itemToArray(BaseTreeItem $item): array
    {
        return $item->toArray();
    }

    /**
     * Рекурсивно добавляет элемент структуры в индекс
     * @param BaseTreeItem $item Элемент структуры
     * @throws \Exception
     */
    private function addItemToIndex(BaseTreeItem $item)
    {
        if ($item->hasChildren()) {
            foreach ($item->getChildren() as $child) {
                $this->addItemToIndex($child);
            }
        }

        $key = $item->getKey();
        if ($this->hasItem($key)) {
            throw new \Exception("Non-unique key \"{$key}\". Key field must be unique.");
        }

        $this->itemsIndex[$key] = $item;
    }

    /**
     * Пероверяет, является ли переданный аргумент валидным объектом - элементом меню
     * @param mixed $item
     * @return bool
     */
    final private function isValidItemObject($item): bool
    {
        $itemClass = $this->getItemClass();
        return $item instanceof $itemClass;
    }
}
