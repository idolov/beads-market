<?php

namespace Idolov\MenuBundle\Model\base;

use Idolov\MenuBundle\DependencyInjection\Configuration;

/**
 * Базовый элемент структуры данных
 */
abstract class BaseTreeItem extends BaseDataSet
{
    /** @var BaseTreeItem Родительский элемент */
    private $parent;

    /** @var BaseTreeItem[] Дочерние элементы */
    private $children;

    /** @var string Kлюч текущего элемента*/
    private $key;

    /**
     * Получает ключ текущего элемента
     * @return string
     */
    public function getKey(): string
    {
        if (!$this->key) {
            $this->key = uniqid('', false);
        }

        return $this->key;
    }

    /**
     * Задает ключ текущего элемента
     * @param string $key Ключ текущего элемента
     * @throws \Exception
     */
    public function setKey(string $key)
    {
        if ($this->key) {
            throw new \Exception('The key is already filled.');
        }
        $this->key = $key;
    }

    /**
     * Проверяет наличие дочерних элементов у текущего элемента
     * @return bool
     */
    public function hasChildren(): bool
    {
        return !empty($this->children);
    }

    /**
     * Получает дочерние элементы
     * @return BaseTreeItem[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Добавляет дочерние элементы текущему
     * @param BaseTreeItem[] $children Дочерние элементы
     * @throws \Exception
     */
    public function setChildren(array $children)
    {
        foreach ($children as $child) {
            $this->addChild($child);
        }
    }

    /**
     * Добавляет дочерний элемент в текущий
     * @param BaseTreeItem $child Дочерний элемент
     * @throws \Exception
     */
    public function addChild($child)
    {
        if (!($child instanceof static)) {
            throw new \Exception('The parameter $child must be a instance of '
                . static::class);
        }

        $child->setParent($this);

        if ($this->children === null) {
            $this->children = [];
         }

        $this->children[] = $child;
    }

    /**
     * Получает родительский элемент по отношению к текущему
     * @return BaseTreeItem|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Задает родительский элемент
     * @param BaseTreeItem $parent Родительский элемент
     * @throws \Exception
     */
    public function setParent($parent)
    {
        if (!($parent instanceof static)) {
            throw new \Exception('The parameter $parent must be a instance of '
                . static::class);
        }

        $this->parent = $parent;
    }

    /**
     * @inheritdoc
     */
    public function toArray(callable $actionBefore = null)
    {
        $self = parent::toArray($actionBefore);
        if ($this->hasChildren()) {
            $self[Configuration::NODE_CHILDREN] = [];
            /** @var BaseTreeItem $child */
            foreach ($this->getChildren() as $child) {
                $self[Configuration::NODE_CHILDREN][] = $child->toArray($actionBefore);
            }
        }

        return $self;
    }
}
