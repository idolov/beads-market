<?php

namespace Idolov\MenuBundle\Model\base;

/**
 * Абстрактный класс базовой модели для элементов меню и хлебных крошек.
 * Хранит в себе набор заранее определенных полей (FieldNames).
 */
abstract class BaseDataSet
{
    /** @var array Атрибуты модели */
    private $_attributes = [];

    /**
     * Получает список свойств данного объекта
     * @return array
     */
    abstract protected function getFieldNames();

    /**
     * При создании, атрибуты заполняется только теми данными,
     * из общей массы, которые возвращает метод getFieldNames данного объекта.
     * @param array $data Массив с данными
     */
    public function __construct(array $data)
    {
        $attributesNames = $this->getFieldNames();
        foreach ($attributesNames as $attributeName) {
            $this->_attributes[$attributeName] = $data[$attributeName] ?? null;
        }
    }

    /**
     * Проверяет, существует ли атрибут с указаанным именем.
     * @param string $name Имя атрибута
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->_attributes[$name]);
    }

    /**
     * Устанавливает атрибут в заданное занчение
     * @param string $name Имя атрибута
     * @param mixed $value Значение атрибута
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->_attributes)) {
            $this->_attributes[$name] = $value;
        }
    }

    /**
     * Получает значение атрибута
     * @param string $name Имя атрибута
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->getAttribute($name);
    }

    /**
     * Получает значения всех атрибутов. Если атрибут не был заполнен, он будет указан в массиве и будет равен null
     * @return array
     */
    public function getAttributes()
    {
        $attributes = [];
        $attributesNames = $this->getFieldNames();
        foreach ($attributesNames as $attributeName) {
            $attributes[$attributeName] = $this->getAttribute($attributeName);
        }

        return $attributes;
    }

    /**
     * Конвертирует модель в массив
     * @param callable|null $actionBefore Действия выполняемые над объектом перед ковертацией в массив
     * @return array
     */
    public function toArray(callable $actionBefore = null)
    {
        if (is_callable($actionBefore)) {
            $actionBefore($this);
        }

        return $this->getAttributes();
    }

    /**
     * Получает значение атрибута, если оно не является пустым
     * @param $name
     * @return mixed|null
     */
    protected function getAttribute($name)
    {
        $attribute = !empty($this->_attributes[$name]) ? $this->_attributes[$name] : null;

        if (($attribute === null) && in_array($name, $this->getFieldNames())) {
            try {
                $getMethod = new \ReflectionMethod(static::class, 'get' . ucfirst($name));
                if ($getMethod->isPublic()
                    && !$getMethod->isStatic()
                ) {
                    $attribute = $getMethod->invoke($this);
                }
            } catch (\ReflectionException $e) {
                return null;
            }
        }

        return $attribute;
    }
}

