<?php

namespace Idolov\MenuBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Бандл для создания и отображения различных меню, а так же хлебных крошек
 */
class IdolovMenuBundle extends Bundle
{

}
