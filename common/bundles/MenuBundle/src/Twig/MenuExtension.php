<?php

namespace Idolov\MenuBundle\Twig;

use Idolov\MenuBundle\Model\menu\Menu;
use Idolov\MenuBundle\Service\BreadcrumbsProvider;
use Idolov\MenuBundle\Service\MenuProvider;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Расширение Twig. Позволяет рендерить меню и хлебные крошки в шаблонах,
 * а так же добавлять новые элементы.
 */
class MenuExtension extends \Twig_Extension
{
    /** @var MenuProvider Сервис управления меню */
    private $menuProvider;

    /** @var BreadcrumbsProvider Сервис управления хлебными крошками */
    private $breadcrumbsProvider;

    /** @var ContainerInterface */
    private $container;

    /**
     * @param ContainerInterface $container
     * @param MenuProvider $menuProvider Сервис управления меню
     * @param BreadcrumbsProvider $breadcrumbsProvider Сервис управления хлебными крошками
     */
    public function __construct(
        ContainerInterface $container,
        MenuProvider $menuProvider,
        BreadcrumbsProvider $breadcrumbsProvider
    ) {
        $this->container = $container;
        $this->menuProvider = $menuProvider;
        $this->breadcrumbsProvider = $breadcrumbsProvider;
    }

    /**
     * Рендерит меню
     * @param string $menuName Идентефикатор меню
     * @return string
     * @throws \Exception
     */
    public function renderMenu($menuName)
    {
        /** @var Menu $menu */
        $menu = $this->menuProvider->getMenu($menuName);

        if (!$menu) {
            throw new \Exception("Не найдено меню $menuName");
        }

        return $this->container->get('twig')->render($menu->getTemplateName(), [
            'menu' => $menu->toArray()
        ]);
    }

    /**
     * Рендерит хлебные крошки
     * @return string
     */
    public function renderBreadcrumbs()
    {
        return $this->container->get('twig')->render($this->breadcrumbsProvider->getTemplateName(), [
            'breadcrumbs' => $this->breadcrumbsProvider->getBreadcrumbs()->toArray()
        ]);
    }

    /**
     * Добавляет новую хлебную крошку
     * @param string $title Заголовок хлебной крошки
     * @param string $url URL Для ссылки
     * @throws \Exception
     */
    public function addBreadcrumb($title, $url)
    {
        $this->breadcrumbsProvider->addCrumb([
            'title' => $title,
            'url' => $url
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            'insert_menu' => new \Twig_SimpleFunction('insert_menu',
                [$this, 'renderMenu'],
                ['is_safe' => ['html']]
            ),
            'insert_breadcrumbs' => new \Twig_SimpleFunction('insert_breadcrumbs',
                [$this, 'renderBreadcrumbs'],
                ['is_safe' => ['html']]
            ),
            'breadcrumb' => new \Twig_SimpleFunction('breadcrumb',
                [$this, 'addBreadcrumb'],
                ['is_safe' => ['html']]
            )
        ];
    }
}
