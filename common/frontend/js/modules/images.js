// Рекурсивно добавляет все изображения из папки frontend/common/images,
// чтобы webpack мог включить их в build/manifest.js и скопировать
// все файлы в build/frontend/common/images
const imagesContext = require.context('../../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);
