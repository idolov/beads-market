let Encore = require('@symfony/webpack-encore');
const glob = require('glob');
const path = require('path');

class AppModule {

    constructor(moduleDir, baseDir) {
        this.moduleDir = moduleDir;
        this.baseDir = baseDir;

        this.indexFilesToEntry();
        this.overFilesToEntry();
    }

    /**
     *  Формирует из индексного файла, входной файл (entry).
     *  Файлы в директориях modules игнорируются (должны быть подключены в entry).
     * Пример: Encore.addEntry('frontend/admin/pages/page-name', './frontend/admin/pages/page-name/index.js');
     * сформирует файл web/build/frontend/admin/pages/page-name.js
     */
    indexFilesToEntry() {
        const self = this;
        const find = [
            'index.js',
            'index.scss'
        ];

        glob.sync(
            this.moduleDir + '/**/{' + find.join() + '}',
            {ignore: this.moduleDir + '/**/modules/**'}
        ).forEach(function (indexFile) {
            const parsedPath = path.parse(indexFile);
            const indexPath = path.join(self.baseDir, indexFile);
            const outputFile = path.dirname(indexFile);

            if (parsedPath.ext === '.js') {
                Encore.addEntry(outputFile, indexPath);
            } else if (parsedPath.ext === '.scss') {
                Encore.addStyleEntry(outputFile, indexPath);
            }
        });

        return this;
    }

    /**
     *  Формирует из js или scss файлов, входные файлы (entry).
     *  Файлы в директориях modules игнорируются (должны быть подключены в entry).
     * Пример: Encore.addEntry('frontend/admin/pages/file', './frontend/admin/pages/file.scss');
     * сформирует файл web/build/frontend/admin/app.js
     */
    overFilesToEntry() {
        const self = this;
        const find = [
            '*.js',
            '*.scss'
        ];

        const ignore = [
            'modules/**',
            'webpack.config.js',
            'index.js',
            'index.scss',
        ];

        glob.sync(
            this.moduleDir + '/**/{' + find.join() + '}',
            {ignore: this.moduleDir + '/**/{' + ignore.join() + '}'
        }).forEach(function (inputFile) {
            const parsedPath = path.parse(inputFile);
            const inputPath = path.join(self.baseDir, inputFile);
            const outputFile = parsedPath.dir + '/' + parsedPath.name;

            if (parsedPath.ext === '.js') {
                Encore.addEntry(outputFile, inputPath);
            } else if (parsedPath.ext === '.scss') {
                Encore.addStyleEntry(outputFile, inputPath);
            }
        });

        return this;
    }
}

module.exports = AppModule;
