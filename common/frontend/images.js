/* todo вынести это в AppModule и удалить файл */

// Рекурсивно добавляет все изображения из папки frontend/admin/images,
// чтобы webpack мог включить их в build/manifest.js и скопировать
// все файлы в build/frontend/admin/images
const imagesContext = require.context('../../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);
