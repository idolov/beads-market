<?php

namespace Common\events\interfaces;

use Common\events\AjaxRequestSubscriber;

/**
 * Интерфейс контроллеара, который принимает только ajax запросы
 * @see AjaxRequestSubscriber
 */
interface OnlyAjaxRequestsInterface
{

}
