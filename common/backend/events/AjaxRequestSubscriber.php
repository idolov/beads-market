<?php

namespace Common\events;

use Common\events\interfaces\OnlyAjaxRequestsInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Добавляет всем экшнам контроллера проверку, что выполняется ajax запрос
 */
class AjaxRequestSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onBeforeAction',
        ];
    }

    /**
     * @param ControllerEvent $event
     */
    public function onBeforeAction(ControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        $controller = array_shift($controller);
        if ($controller instanceof OnlyAjaxRequestsInterface) {

            $request = $event->getRequest();
            if ($request && !$request->isXmlHttpRequest()) {
                throw new BadRequestHttpException('This action accepts only ajax requests.');
            }
        }
    }
}
