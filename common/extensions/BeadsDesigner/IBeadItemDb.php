<?php
namespace Idolov\BeadsDesigner;

interface IBeadItemDb
{
    public function getPrice();
    public function getTitle();
}
