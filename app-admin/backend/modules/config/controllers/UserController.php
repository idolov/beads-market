<?php
namespace AppAdmin\modules\config\controllers;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    /**
     * @Route("/config/user", name="user")
     */
    public function indexAction()
    {
        return $this->render('config/user/index.html.twig');
    }
}
