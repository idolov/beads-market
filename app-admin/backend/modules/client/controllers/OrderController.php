<?php
namespace AppAdmin\modules\client\controllers;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OrderController extends Controller
{
    /**
     * @Route("/client/order", name="order_index")
     */
    public function indexAction()
    {
        return $this->render('client/order/index.html.twig');
    }
}
