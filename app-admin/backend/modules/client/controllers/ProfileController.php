<?php
namespace AppAdmin\modules\client\controllers;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProfileController extends Controller
{
    /**
     * @Route("/client/profile", name="profile")
     */
    public function indexAction()
    {
        return $this->render('client/profile/index.html.twig');
    }
}
