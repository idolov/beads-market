<?php
namespace AppAdmin\modules\supply\controllers;

use App\Entity\Supplier;
use AppAdmin\controllers\base\AdminController;
use AppAdmin\modules\supply\forms\SupplierType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SupplierController extends AdminController
{
    /**
     * @Route("/supply/supplier", name="supplier")
     */
    public function index(): Response
    {
        $suppliers = $this->getDoctrine()
            ->getRepository(Supplier::class)
            ->findAll();

        return $this->render('supply/supplier/index.html.twig', ['suppliers' => $suppliers]);
    }

    /**
     * @Route("/new", name="supplier_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $supplier = new Supplier();
        $form = $this->createForm(SupplierType::class, $supplier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($supplier);
            $em->flush();

            return $this->redirectToRoute('supplier_index');
        }

        return $this->render('supply/supplier/new.html.twig', [
            'supplier' => $supplier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="supplier_show", methods="GET")
     */
    public function show(Supplier $supplier): Response
    {
        return $this->render('supply/supplier/show.html.twig', ['supplier' => $supplier]);
    }

    /**
     * @Route("/{id}/edit", name="supplier_edit", methods="GET|POST")
     */
    public function edit(Request $request, Supplier $supplier): Response
    {
        $form = $this->createForm(SupplierType::class, $supplier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('supplier_show', ['id' => $supplier->getId()]);
        }

        return $this->render('supply/supplier/edit.html.twig', [
            'supplier' => $supplier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="supplier_delete", methods="DELETE")
     */
    public function delete(Request $request, Supplier $supplier): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($supplier);
        $em->flush();

        return $this->redirectToRoute('supplier_index');
    }

    public function getMainMenuActiveKey(): ?string
    {
        return 'supplier_index';
    }
}
