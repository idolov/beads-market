<?php
namespace AppAdmin\modules\catalog\controllers;

use App\Entity\Category;
use Common\events\interfaces\OnlyAjaxRequestsInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CategoryAjaxController extends AbstractController implements OnlyAjaxRequestsInterface
{
    /**
     * @Route("/catalog/category/{id}/form", name="category_form", methods="GET")
     */
    public function editAction(Request $request, Category $category)
    {
        
        var_dump($category->getName());
        exit;

        return $this->renderView('catalog/category/_form.html.twig');
    }
}
