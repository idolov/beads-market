<?php
namespace AppAdmin\modules\catalog\controllers;

use App\Entity\Product;
use App\Entity\ProductImage;
use App\Repository\ProductImageRepository;
use AppAdmin\controllers\base\AdminController;
use AppAdmin\modules\catalog\forms\product\ProductType;
use Gedmo\Uploadable\FileInfo\FileInfoArray;
use Stof\DoctrineExtensionsBundle\Uploadable\UploadableManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AdminController
{
    /** Идентификатор меню для раздела редактирования товара */
    const MENU_PRODUCT_EDIT = 'product_edit';

    /**
     * @Route("/catalog/product", name="product_index", methods="GET")
     */
    public function index(): Response
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();

        return $this->render('catalog/product/index.html.twig', ['products' => $products]);
    }

    /**
     * @Route("/catalog/product/new", name="product_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('product_edit', ['id' => $product->getId()]);
        }

        return $this->render('catalog/product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/catalog/product/{id}/show", name="product_show", methods="GET")
     */
    public function show(Product $product): Response
    {
        return $this->render('catalog/product/show.html.twig', ['product' => $product]);
    }

    /**
     * @Route("/catalog/product/{id}/edit", name="product_edit", methods="GET|POST")
     * @throws \Exception
     */
    public function edit(Request $request, Product $product): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);
        $this->initEditPageMenu($request, $product);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product_edit', ['id' => $product->getId()]);
        }

        return $this->render('catalog/product/edit/main.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/catalog/product/{id}/edit/images", name="product_edit_images", methods="GET")
     * @throws \Exception
     */
    public function editImages(Request $request, Product $product): Response
    {
        $this->initEditPageMenu($request, $product);

        /** @var ProductImageRepository $images */
        $images = $this->getDoctrine()
            ->getRepository(ProductImage::class);

        return $this->render('catalog/product/edit/images.html.twig', [
            'product' => $product,
            'images' => $images->getImagesByProduct($product),
        ]);
    }

    /**
     * @Route("/catalog/product/{id}/upload/images", name="product_upload_images", methods="POST")
     * @throws \Exception
     */
    public function uploadImages(Request $request, Product $product, UploadableManager $uploadableManager): Response
    {
        $fileInfo = $_FILES['filepond'] ?? null;
        if ($request->isMethod('POST') && is_array($fileInfo)) {
            $image = new ProductImage();
            $image->setProduct($product);

            $em = $this->getDoctrine()->getManager();
            $connection = $this->getDoctrine()->getConnection();

            $connection->beginTransaction();
            try {
                $uploadableManager->markEntityToUpload($image, new FileInfoArray($fileInfo));
                $em->persist($image);
                $em->flush();
                $connection->commit();
            } catch (\Exception $e) {
                $connection->rollBack();
                throw $e;
            }

            return $this->json([
                'id' => $image->getId(),
                'url' => $image->getUrl(),
            ]);
        }
        return new Response('Отсутствуют данные для загрузки', 422);
    }

    /**
     * @Route("/catalog/product/image/{id}/delete", name="product_delete_image", methods="GET|DELETE")
     * @throws \Exception
     */
    public function deleteImage(ProductImage $productImage): Response
    {
        $product = $productImage->getProduct();

        $em = $this->getDoctrine()->getManager();
        $em->remove($productImage);
        $em->flush();

        return $this->redirectToRoute('product_edit_images', ['id' => $product->getId()]);
    }

    /**
     * Помечает изображение как основное
     * @Route("/catalog/product/image/{id}/main", name="product_main_image", methods="GET")
     * @throws \Exception
     */
    public function mainImage(ProductImage $productImage): Response
    {
        $product = $productImage->getProduct();

        /** @var ProductImageRepository $images */
        $images = $this->getDoctrine()
            ->getRepository(ProductImage::class);

        $images->markAsMain(
            $productImage,
            $this->getDoctrine()->getManager()
        );

        return $this->redirectToRoute('product_edit_images', ['id' => $product->getId()]);
    }

    /**
     * @Route("/catalog/product/{id}/edit/options", name="product_edit_options", methods="GET|POST")
     * @throws \Exception
     */
    public function editOptions(Request $request, Product $product): Response
    {
        $this->initEditPageMenu($request, $product);

        return $this->render('catalog/product/edit/options.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/catalog/product/{id}/edit/suppliers", name="product_edit_suppliers", methods="GET|POST")
     * @throws \Exception
     */
    public function editSuppliers(Request $request, Product $product): Response
    {
        $this->initEditPageMenu($request, $product);

        return $this->render('catalog/product/edit/suppliers.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/catalog/product/{id}", name="product_delete", methods="GET|DELETE")
     */
    public function delete(Product $product): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('product_index');
    }

    /**
     * @inheritdoc
     */
    public function getMainMenuActiveKey(): ?string
    {
        return 'product_index';
    }

    /**
     * Инициализирует меню раздела редактирования товара
     * @param Request $request
     * @param Product $product
     * @throws \Exception
     */
    private function initEditPageMenu(Request $request, Product $product): void
    {
        $routeParams = ['id' => $product->getId()];
        $this->initMenu(self::MENU_PRODUCT_EDIT)
            ->addItem([
                'key' => 'product_edit',
                'title' => 'Общее',
                'url' => $this->generateUrl('product_edit', $routeParams),
                'icon' => 'ti-crown'
            ])
            ->addItem([
                'key' => 'product_edit_images',
                'title' => 'Изображения',
                'url' => $this->generateUrl('product_edit_images', $routeParams),
                'icon' => 'ti-image'
            ])
            ->addItem([
                'key' => 'product_edit_options',
                'title' => 'Характеристики',
                'url' => $this->generateUrl('product_edit_options', $routeParams),
                'icon' => 'ti-panel'
            ])
            ->addItem([
                'key' => 'product_edit_suppliers',
                'title' => 'Поставщики',
                'url' => $this->generateUrl('product_edit_suppliers', $routeParams),
                'icon' => 'ti-truck'
            ])
            ->setActiveItemKey($request->get('_route'));
    }
}
