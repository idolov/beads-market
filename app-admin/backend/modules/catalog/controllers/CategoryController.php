<?php
namespace AppAdmin\modules\catalog\controllers;

use AppAdmin\controllers\base\AdminController;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AdminController
{
    /**
     * @Route("/catalog/category", name="product")
     */
    public function indexAction()
    {
        return $this->render('catalog/category/index.html.twig');
    }

    /**
     * @inheritdoc
     */
    public function getMainMenuActiveKey(): ?string
    {
        return 'catalog_category_index';
    }
}
