<?php

namespace AppAdmin\modules\catalog\forms\product;

use App\Entity\Product;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Title'
            ])
            ->add('price', MoneyType::class, [
                'currency' => 'RUB',
            ])
            ->add('notice', CKEditorType::class, ['config_name' => 'notice'])
            ->add('description', CKEditorType::class, [
                'config_name' => 'description',
                'config' => [
                    'height' => 300
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
