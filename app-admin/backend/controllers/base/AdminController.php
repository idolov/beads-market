<?php

namespace AppAdmin\controllers\base;

use AppAdmin\Constants;
use AppAdmin\models\menu\PageMenu;
use Idolov\MenuBundle\Controller\MenuInterface;
use Idolov\MenuBundle\Controller\MenuInControllerTrait;
use Idolov\MenuBundle\Service\MenuProvider;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Базовый контроллер приложения admin
 */
abstract class AdminController extends AbstractController implements MenuInterface
{
    use MenuInControllerTrait;

    /**
     * @inheritdoc
     */
    public function getMainMenuName(): ?string
    {
        return Constants::MENU_MAIN;
    }

    /**
     * @inheritdoc
     */
    protected function getContainer(): ?ContainerInterface
    {
        return $this->container;
    }

    /**
     * @inheritdoc
     */
    protected function getMenuClassName()
    {
        return PageMenu::class;
    }

    public static function getSubscribedServices()
    {
        return array_merge(parent::getSubscribedServices(), [
            Constants::SERVICE_MENU => '?' . MenuProvider::class,
        ]);
    }
}
