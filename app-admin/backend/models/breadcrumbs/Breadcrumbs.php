<?php

namespace AppAdmin\models\breadcrumbs;

/**
 * Хлебные крошки приложения admin
 */
class Breadcrumbs extends \Idolov\MenuBundle\Model\breadcrumbs\Breadcrumbs
{
    /**
     * @inheritdoc
     */
    public function getTemplateName(): string
    {
        return 'common/breadcrumbs/breadcrumbs.html.twig';
    }
}
