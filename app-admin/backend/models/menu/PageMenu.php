<?php

namespace AppAdmin\models\menu;

use Idolov\MenuBundle\Model\menu\MenuInController;

/**
 * Меню страницы (раздела)
 */
class PageMenu extends MenuInController
{
    /**
     * @inheritdoc
     */
    protected function getItemClass(): string
    {
        return PageMenuItem::class;
    }

    /**
     * @inheritdoc
     */
    public function getTemplateName(): string
    {
        return 'common/menu/page_menu.html.twig';
    }
}

