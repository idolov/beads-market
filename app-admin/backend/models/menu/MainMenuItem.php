<?php

namespace AppAdmin\models\menu;

use Idolov\MenuBundle\Model\menu\MenuItem;

/**
 * Элемент главного меню приложения admin
 * @property string $key Ключ текущего элемента
 * @property string $icon Иконка
 * @property string $title Заголовок элемента меню
 * @property string $route Маршрут, для генерции URL
 */
class MainMenuItem extends MenuItem
{
    /**
     * @inheritdoc
     */
    protected function getFieldNames()
    {
        return array_merge([
            'icon',
        ], parent::getFieldNames());
    }
}
