<?php

namespace AppAdmin\models\menu;

use Idolov\MenuBundle\Model\menu\Menu;

/**
 * Главное меню приложения admin
 */
class MainMenu extends Menu
{
    /**
     * @inheritdoc
     */
    protected function getItemClass(): string
    {
        return MainMenuItem::class;
    }

    /**
     * @inheritdoc
     */
    public function getTemplateName(): string
    {
        return 'common/menu/main_menu.html.twig';
    }

}
