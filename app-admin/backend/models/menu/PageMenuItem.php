<?php

namespace AppAdmin\models\menu;

use Idolov\MenuBundle\Model\menu\MenuInControllerItem;

/**
 * Элемент меню страницы (раздела)
 * @property string $key Ключ текущего элемента
 * @property string $icon Иконка
 * @property string $title Заголовок элемента меню
 * @property string $route Маршрут, для генерции URL
 * @property string $url URL
 */
class PageMenuItem extends MenuInControllerItem
{
    /**
     * @inheritdoc
     */
    protected function getFieldNames()
    {
        return array_merge([
            'icon',
        ], parent::getFieldNames());
    }
}
