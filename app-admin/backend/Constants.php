<?php

namespace AppAdmin;

/**
 * Общие константы приложения администрирования
 */
class Constants
{
    /** Идентификатор основного меню приложения в сервисе idolov.menu_provider */
    const MENU_MAIN = 'main_menu';

    /** Сервисы: */
    /** Сервис обслуживания различных меню */
    const SERVICE_MENU = 'idolov.menu_provider';
    /** Сервис обслуживания хлебных крошек */
    const SERVICE_BREADCRUMBS = 'idolov.breadcrumbs_provider';
}
