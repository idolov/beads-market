<?php

namespace AppAdmin\bundles\SecurityBundle\DependencyInjection;

use App\Entity\User;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\NodeInterface;

class Configuration implements ConfigurationInterface
{
    /** Класс-отношение для получения юзера из базы данных */
    const USER_ENTITY_CLASS = User::class;

    /**
     * Generates the configuration tree.
     *
     * @return NodeInterface
     */
    public function getConfigTreeBuilder()
    {
        return new TreeBuilder();
    }
}
