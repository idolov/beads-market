<?php

namespace AppAdmin\bundles\SecurityBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;

class AdminSecurityExtension extends Extension
{
    /**
     * Loads a specific configuration.
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new Loader\YamlFileLoader(
            $container,
            new FileLocator (__DIR__ . '/../config')
        );
        $loader->load('services.yml');
    }
}
