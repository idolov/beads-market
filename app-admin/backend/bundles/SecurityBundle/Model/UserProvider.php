<?php

namespace AppAdmin\bundles\SecurityBundle\Model;

use App\Entity\User;
use AppAdmin\bundles\SecurityBundle\DependencyInjection\Configuration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Представляет класс, который загружает объекты UserInterface из некоторого источника для системы аутентификации.
 * В данном случае для получения данных юзера из базы данных используется класс App\Entity\User и хранилище
 */
class UserProvider implements UserProviderInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Symfony calls this method if you use features like switch_user
     * or remember_me.
     *
     * If you're not using these features, you do not need to implement
     * this method.
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        /** @var User $user */
        $user = $this->getRepository()->findOneBy(['email' => $username]);

        if (!$user) {
            // fail authentication with a custom error
            throw new UsernameNotFoundException('User could not be found.');
        }

        return new AdminUser($user);
    }

    /**
     * Refreshes the user after being reloaded from the session.
     *
     * When a user is logged in, at the beginning of each request, the
     * User object is loaded from the session and then this method is
     * called. Your job is to make sure the user's data is still fresh by,
     * for example, re-querying for fresh User data.
     *
     * If your firewall is "stateless: false" (for a pure API), this
     * method is not called.
     *
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof AdminUser) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        $repository = $this->getRepository();

        // The user must be reloaded via the primary key as all other data
        // might have changed without proper persistence in the database.
        // That's the case when the user has been changed by a form with
        // validation errors.
        if (!$id = $this->getClassMetadata()->getIdentifierValues($user->getUserEntity())) {
            throw new \InvalidArgumentException('You cannot refresh a user '.
                'from the EntityUserProvider that does not contain an identifier. '.
                'The user object has to be serialized with its own identifier '.
                'mapped by Doctrine.'
            );
        }

        /** @var User $refreshedUser */
        $refreshedUser = $repository->find($id);
        if (null === $refreshedUser) {
            throw new UsernameNotFoundException(sprintf('User with id %s not found', json_encode($id)));
        }

        return new AdminUser($refreshedUser);
    }

    /**
     * Tells Symfony to use this provider for this User class.
     */
    public function supportsClass($class)
    {
        return AdminUser::class === $class;
    }

    /**
     * Получает класс-хранилище для отношения User
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    private function getRepository()
    {
        return $this->entityManager->getRepository(Configuration::USER_ENTITY_CLASS);
    }

    /**
     * Получает метаданные отношения User
     * @return \Doctrine\ORM\Mapping\ClassMetadata
     */
    private function getClassMetadata()
    {
        return $this->entityManager->getClassMetadata(Configuration::USER_ENTITY_CLASS);
    }
}
