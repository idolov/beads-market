<?php

namespace AppAdmin\bundles\SecurityBundle\Model;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Слушает событие kernel.request. Проверяет, если авторизованный пользователь пытается получить доступ к страницам,
 * к которым есть доступ только для неавторизованных пользователей, например /login, то редиректит на главную.
 */
class RedirectUserListener
{
    private $tokenStorage;
    private $router;

    public function __construct(TokenStorageInterface $t, RouterInterface $r)
    {
        $this->tokenStorage = $t;
        $this->router = $r;
    }

    /**
     * Обработчик события ядра "request"
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if ($this->isUserLogged() && $event->isMasterRequest()) {
            $currentRoute = $event->getRequest()->attributes->get('_route');
            if ($this->isAnonymousPage($currentRoute)) {
                $response = new RedirectResponse($this->router->generate('index'));
                $event->setResponse($response);
            }
        }
    }

    /**
     * Проверяет, является ли пользователь авторизованным
     * @return bool
     */
    private function isUserLogged()
    {
        $token = $this->tokenStorage->getToken();
        $user = $token ? $token->getUser() : null;
        return $user instanceof AdminUser;
    }

    /**
     * Проверяет, ведет ли тикущий роут на страницу, куда есть доступ только для анонимусов.
     * @param string $currentRoute
     * @return bool
     */
    private function isAnonymousPage($currentRoute)
    {
        return $currentRoute === 'login';
    }
}
