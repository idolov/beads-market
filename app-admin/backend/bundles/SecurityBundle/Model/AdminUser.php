<?php

namespace AppAdmin\bundles\SecurityBundle\Model;

use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Используется компонентами symfony scurity для авторизации и аутентификации пользователей в приложении admin
 */
class AdminUser implements UserInterface
{
    /** @var User Сущность БД user */
    private $userEntity;

    /**
     * @param User $userEntity Сущность БД user
     */
    public function __construct(User $userEntity)
    {
        $this->userEntity = $userEntity;
    }

    /**
     * A visual identifier that represents this user.
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return $this->userEntity->getEmail();
    }

    /**
     * Returns the password used to authenticate the user.
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     * @return string The password
     */
    public function getPassword()
    {
        return $this->userEntity->getPassword();
    }

    /**
     * Returns the roles granted to the user.
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $admin = $this->getAdmin();
        $roles = $admin ? $admin->getRoles() : [];

        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * Setts the roles granted to the user.
     * @param array $roles
     * @return AdminUser
     */
    public function setRoles(array $roles): self
    {
        $admin = $this->getAdmin();
        if ($admin) {
            $admin->setRoles($roles);
       }
        return $this;
    }

    /**
     * Получает аккаунт администратора, если он имеется у текущего пользователя
     * @return \App\Entity\Admin|null
     */
    public function getAdmin()
    {
        return $this->userEntity->getAdmin();
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * Получает сущность БД user
     * @return User
     */
    public function getUserEntity(): User
    {
        return $this->userEntity;
    }
}
