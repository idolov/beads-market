<?php

namespace AppAdmin\bundles\SecurityBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Все, что касается ограничения доступа (login, logout, и тд) в приложении admin
 * @see config/packages/security.yaml
 * @author Dolgov Ivan <vanish.d@yandex.ru>
 */
class AdminSecurityBundle extends Bundle
{

}
