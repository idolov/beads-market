
export default class SidebarMethods {
    /**
     * Получает Juery-объект приложения
     */
    getApp() {
        return $('.app');
    }

    /**
     * Открывает боковые меню
     */
    openSidebar() {
        return this.getApp().addClass('is-collapsed');
    }

    /**
     * Закрывает боковые меню
     */
    closeSidebar() {
        return new Promise((resolve) => {
            this.getApp().removeClass('is-collapsed');
            setTimeout(() => {
                resolve();
            }, 50); // Небольшая задержка, чтобы меню успело закрыться до выполнения других действий
        });
    }

    /**
     * Переключает боковые меню из открытого состояния в закрытое и наоборот
     */
    toggleSidebar() {
        return new Promise((resolve) => {
            this.getApp().toggleClass('is-collapsed');
            setTimeout(() => {
                resolve();
            }, 50); // Небольшая задержка, чтобы меню успело закрыться до выполнения других действий
        });
    }

    /**
     * Закрывает все выползающие панели
     */
    closeCrawlBars() {
        $('.crawl-down-bar').hide();
        $('.crawl-down-btn').removeClass('active');
    }

    /**
     * Закрывает все выползающие панели и открывает меню
     */
    openSidebarMenu() {
        return $('.sidebar-menu').show();
    }

    /**
     * Закрывает все выползающие панели и закрывает меню
     */
    closeSidebarMenu() {
        return $('.sidebar-menu').hide();
    }

    /**
     * Закрывает хлебные крошки
     */
    closeBreadcrumbs() {
        return $('.header-container .nav-left').removeClass('collapsed');
    }
};
