import SidebarMethods from './sidebarMethods';

export default (function () {
    let sidebar = new SidebarMethods();

    /**
     * Клик по пункту меню
     */
    $('.sidebar .sidebar-menu li a').on('click', function () {
        const $this = $(this);

        if ($this.parent().hasClass('open')) {
            $this
                .parent()
                .children('.dropdown-menu')
                .slideUp(200, () => {
                    $this.parent().removeClass('open');
                });
        } else {
            $this
                .parent()
                .children('.dropdown-menu')
                .slideDown(200, () => {
                    $this.parent().addClass('open');
                });
        }
    });

    /**
     * Открытие родительского пункта меню с активным дочерним элементом
     */
    $('.sidebar .sidebar-link.active').closest('.dropdown').find('a.top-level').click();

    /**
     * Переключение бокового меню из открытого состояния в закрытое и наоборот
     */
    $('.sidebar-toggle').on('click', function (e) {
        e.preventDefault();
        return async function () {
            await sidebar.toggleSidebar();
            await sidebar.closeCrawlBars();
            await sidebar.openSidebarMenu();
        }();
    });

    /**
     * Wait untill sidebar fully toggled (animated in/out)
     * then trigger window resize event in order to recalculate
     * masonry layout widths and gutters.
     */
    $('#sidebar-toggle').click(e => {
        e.preventDefault();
        setTimeout(() => {
            window.dispatchEvent(window.EVENT);
        }, 300);
    });

    /**
     * Развертывание хлебных крошек
     */
    $('.breadcrumb-item-toggle').on('click', function (e) {
        e.stopPropagation();
        $(this).closest('.header-container .nav-left').addClass('collapsed');
    });

    /**
     * Свертывание хлебных крошек
     */
    $('.breadcrumb-close').on('click', function (e) {
        e.stopPropagation();
        sidebar.closeBreadcrumbs();
    });

    /**
     * Закрытие различных меню и панелей, при клике по любому месту на странице
     */
    $(document).on('click', function (e) {
        e.stopPropagation();
        if ($(e.target).closest('.header-container  .nav-left, .nav-sidebar-toggle, .sidebar, .actionbar').length) {
            return;
        }
        sidebar.closeBreadcrumbs();
        return async function () {
            await sidebar.closeSidebar();
            await sidebar.closeCrawlBars();
            await sidebar.openSidebarMenu();
        }();
    });

    /**
     * Нажатие на кнопку открытия какой-либо выползающей панели
     */
    $('.crawl-down-btn').on('click', function (e) {
        e.stopPropagation();
        const $this = $(this);

        const dataTarget = $this.attr('data-target');
        if (!dataTarget) {
            console.error('Укажите data-target атрибут у кнопки', $this);
            return;
        }

        if ($this.hasClass('active')) {
            sidebar.closeCrawlBars();
            sidebar.openSidebarMenu();
            return;
        }

        let $crawlDownBar = $(dataTarget);
        if (!$crawlDownBar.length) {
            console.error('data-target элемент не найден', dataTarget);
            return;
        }

        const dataSource = $this.attr('data-source');
        if (dataSource) {
            // todo Реализовать логику подгрузки данных
        }

        sidebar.closeCrawlBars();
        sidebar.closeSidebarMenu();
        $this.addClass('active');
        if ($('.sidebar').offset().left < 0) {
            sidebar.openSidebar();
        }
        $crawlDownBar.show();
    });

    /**
     * Нажатие на кнопку закрытия выползающей панели
     */
    $('.crawl-down-close').on('click', function () {
        sidebar.closeCrawlBars();
        sidebar.openSidebarMenu();
    })
}());
