import 'node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js';
import 'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css';

export default (function () {
  $('.start-date').datepicker();
  $('.end-date').datepicker();
}())
