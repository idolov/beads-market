(function() {
    /** Инизиализация загрузчика файлов */
    let initFilepond = function () {
        FilePond.setOptions({
            allowDrop: false,
            allowReplace: false,
            allowFileEncode: true,
            instantUpload: true,
            labelIdle: 'Перетащите изображения или нажмите <span class="filepond--label-action"> Загрузить </span>',
            /* Заполняется в целевом шаблоне
            server: {
                process: '{{ path('product_upload_images', {'id': product.id}) }}',
            }
            name: 'filePond',
            */
        });

        FilePond.create(document.querySelector('input[type="file"]'));

        $('body').on('click', '.close-uploads, #upload-modal .close', function (e) {
            location.reload();
        });
    };

    /** Инизиализация кнопок управления изображением */
    let initImageButtons = function () {
        $(document).on('click', function (e) {
            e.stopPropagation();
            if ($(e.target).closest('.image-container').length) {
                return;
            }
            $('.image-container').removeClass('collapsed');
        });

        $('body').on('click', '.image-container', function () {
            $('.image-container').removeClass('collapsed');
            $(this).addClass('collapsed');
        })
    };

    $(document).ready(function () {
        initFilepond();
        initImageButtons();
    });
    
})();
