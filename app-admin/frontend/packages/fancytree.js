// Scripts
import 'node_modules/jquery.fancytree/dist/modules/jquery.fancytree';
import 'node_modules/jquery.fancytree/dist/modules/jquery.fancytree.edit';
import 'node_modules/jquery.fancytree/dist/modules/jquery.fancytree.dnd5';
import 'node_modules/jquery.fancytree/dist/modules/jquery.fancytree.filter';
import 'node_modules/jquery.fancytree/dist/modules/jquery.fancytree.glyph';
global.fancytree = require('jquery.fancytree');

// Styles
import "node_modules/jquery.fancytree/dist/skin-bootstrap/ui.fancytree.less";
